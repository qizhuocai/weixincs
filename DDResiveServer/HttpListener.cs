﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using Aocwes.Model;
using DDResiveServer;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace Aocwes.MainConsole
{
    public class HttpListener
    {
        BugTicketEventHandler dt = null;
        public HttpListener(BugTicketEventHandler dtout)
        {
            dt = dtout;
            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, 8815);
            Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            serverSocket.Bind(ipep);
            serverSocket.Listen(10);
            while (true)
            {
                try
                {
                    var clientSocket = serverSocket.Accept();
                    ThreadPool.QueueUserWorkItem(new WaitCallback(ReceiveData), clientSocket);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("listening Error: " + ex.Message);
                }
            }
        }

        private void ReceiveData(object socket)
        {
            RequestHeader requestHeader = new RequestHeader();
            var clientSocket = socket as Socket;
            Byte[] buffer = new Byte[1024];
            IPEndPoint clientep = (IPEndPoint)clientSocket.RemoteEndPoint;
            string clientMessage = string.Empty;
            try
            {
                clientSocket.Receive(buffer, 0, buffer.Length, SocketFlags.None);
                clientMessage = System.Text.Encoding.Default.GetString(buffer);
                //Console.WriteLine(clientMessage);
                GetUseInfoFormDD(clientMessage);
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("<html><head><title>Server throw an exception:{1}</title></head><body>{0}</body></html>", ex.Message, ex.GetType().FullName);
                var data = Encoding.ASCII.GetBytes(sb.ToString());
                clientSocket.Send(data, data.Length, SocketFlags.None);
            }

            using (StringReader sr = new StringReader(clientMessage))
            {
                StringBuilder sb = new StringBuilder();
                var line = sr.ReadLine();
                if (!string.IsNullOrWhiteSpace(line))
                {
                    string[] headerInfo = line.Split(new char[] { ' ' });
                    if (headerInfo.Length == 3)
                    {
                        requestHeader.SetHeader(headerInfo[0], headerInfo[1], headerInfo[2]);
                        switch (requestHeader.Method.ToUpper())
                        {
                            case "GET":
                                for (line = sr.ReadLine(); !string.IsNullOrWhiteSpace(line); line = sr.ReadLine())
                                {
                                    string[] tokens = line.Split(new string[] { ": " }, StringSplitOptions.None);
                                    if (tokens.Length == 2)
                                    {
                                        requestHeader.Properties.Add(tokens[0], tokens[1]);
                                    }
                                    else
                                    {
                                        Console.WriteLine(line);
                                    }
                                }
                                requestHeader.Protocol = "HTTP/1.1";
                                writesuccess(sb, requestHeader);
                                
                                sb.AppendFormat("<html><head><title>You request {1}</title></head><body><h2>Welcome to aocwes http server</h2>{0}<h3><div><h3>Your requested content:</h3>{2}</div>Author: <a href='mailto:flyear.cheng2gmail.com'>Flyear</a></h3></body></html>", requestHeader.ToString(), requestHeader.Url, clientMessage);
                                var data = Encoding.ASCII.GetBytes(sb.ToString());
                                clientSocket.Send(data, data.Length, SocketFlags.None);
                                Console.WriteLine(string.Format("ClientAddress:{0}, Request Url:{1}", clientep.Address, requestHeader.Url));
                                break;
                            case "POST":
                            case "HEAD":
                            case "PUT":
                            case "DELETE":
                            case "TRACE":
                            case "CONNECT":
                            case "OPTIONS":
                            default:
                                writesuccess(sb, requestHeader);
                                sb.AppendFormat("<html><head><title>You request {1}</title></head><body><h2>Welcome to aocwes http server</h2>{0}<h3>Author: <a href='mailto:flyear.cheng2gmail.com'>Flyear</a></h3></body></html>", requestHeader.ToString(), requestHeader.Url);
                                var data1 = Encoding.ASCII.GetBytes(sb.ToString());
                                clientSocket.Send(data1, data1.Length, SocketFlags.None);

                                break;
                        }
                    }
                    else
                    {
                        Console.WriteLine(line);
                    }
                }
            }
            clientSocket.Shutdown(SocketShutdown.Both);
        }

        private Dictionary<string, object> JsonToDictionary(string jsonData)
        {
            //实例化JavaScriptSerializer类的新实例  
            JavaScriptSerializer jss = new JavaScriptSerializer();
            try
            {
                //将指定的 JSON 字符串转换为 Dictionary<string, object> 类型的对象  
                return jss.Deserialize<Dictionary<string, object>>(jsonData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }  

        private void GetUseInfoFormDD(string msg)
        {
            string s = "\"6\":\\[";
            string e = "\\],\"7\"";//"(?<=(" + s + "))[.\\s\\S]*?(?=(" + e + "))"
            Regex rg = new Regex("(?<=(" + s + "))[.\\s\\S]*?(?=(" + e + "))", RegexOptions.Multiline | RegexOptions.Singleline);
            string  userinfo = rg.Match(msg).Value;

            if (!string.IsNullOrWhiteSpace(userinfo))
            {
                userinfo = userinfo.Trim();
                Dictionary<string, object> dic = JsonToDictionary(userinfo);
                    if(dic.ContainsKey("2"))
                    {
                        string base64str = dic["2"] as string;
                        if(!string.IsNullOrWhiteSpace(base64str))
                        {
                            string strmsg = DecodeBase64(base64str);
                            if (!string.IsNullOrWhiteSpace(strmsg) && dt!=null)
                            {
                                if(!strmsg.Contains("在WX说"))
                                    dt("强哥在DD说："+strmsg);
                            }
                        }
                    }
            }
        }

        public static string DecodeBase64(System.Text.Encoding encode, string result)
        {
            string decode = "";
            byte[] bytes = Convert.FromBase64String(result);
            if (bytes.Length < 70)
            {
                return null;
            }
            try
            {
                //字符串前面目测是56个，后面目测是14个，待验证
                //byte[] bt1 = new byte[]{1,2,3,4,5,6,7};
                //byte[] bt2 = new byte[3];
                //Array.Copy(bt1,3,bt2,0,3);
                //复制 4，5，6
                byte[] bt2 = new byte[bytes.Length - 56];
                Array.Copy(bytes, 56, bt2, 0, bytes.Length - 56);

                byte[] bt3 = new byte[bt2.Length - 14];
                Array.Copy(bt2, 0, bt3, 0, bt2.Length - 14);

                decode = encode.GetString(bt3);
            }
            catch
            {
                decode = result;
            }
            return decode;
        }

        /// <summary>
        /// Base64解密，采用utf8编码方式解密
        /// </summary>
        /// <param name="result">待解密的密文</param>
        /// <returns>解密后的字符串</returns>
        public static string DecodeBase64(string result)
        {
            return DecodeBase64(System.Text.Encoding.UTF8, result);
        }

        public void writesuccess(StringBuilder sw, RequestHeader rh)
        {
            sw.AppendFormat("{0} 200 ok\r\n", rh.Protocol);
            sw.AppendLine("connection: close");
            sw.AppendLine();
        }

        public void writefailure(StringBuilder sw)
        {
            sw.AppendLine("http/1.0 404 file not found");
            sw.AppendLine("connection: close");
            sw.AppendLine();
        }
    }
}
