﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGWeBQQWXSDK
{
  public static   class SysLog
    {
        public class LogManager
        {
            public event EventHandler<CQLogEventArgs> NewLogWrite = null;
            //  public event EventHandler<CQLogEventArgs> NewLogWritea = null;

            private List<string> logMessages = null;
            //private List<string> Messages = null;

            private static LogManager _instance = null;

            private LogManager()
            {
                this.logMessages = new List<string>();
            }

            public static LogManager GetInstance()
            {
                if (_instance == null)
                {
                    _instance = new LogManager();
                }

                return _instance;
            }

            public List<string> Logs
            {
                get
                {
                    return logMessages;
                }
            }


            public void AddLog(string message)
            {
                /*logMessages.Add(message);

                if (logMessages.Count > 100)
                {
                    this.logMessages.RemoveAt(0);
                }*/

                if (this.NewLogWrite != null)
                {
                    this.NewLogWrite(this, new CQLogEventArgs() { LogMessage = message });
                }
            }

        }
    }
}
