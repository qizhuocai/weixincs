﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGWeBQQWXSDK.Http
{
 public static    class JosnModel
    {


        #region 群消息
        /// <summary>
        /// 群消息
        /// </summary>
        public class JsonGroupModel
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                public List<paramGnamelist> gnamelist;
                public class paramGnamelist
                {
                    public string flag;
                    public string gid;
                    public string code;
                    public string name;
                }
            }
        }
        #endregion
        #region 获取群详细信息
        /// <summary>
        /// 获取群详细信息
        /// </summary>
        public class JsonGroupInfoModel
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                public List<paramMinfo> minfo;
                public List<paramCards> cards;
                public paramGinfo ginfo;
                public class paramMinfo
                {
                    public string nick;
                    public string province;
                    public string gender;
                    public string uin;
                    public string country;
                    public string city;
                }
                public class paramCards
                {
                    public string muin;
                    public string card;
                }
                public class paramGinfo
                {
                    public string code;
                    public string createtime;
                    public string flag;
                    public string name;
                    public string gid;
                    public string owner;
                    public List<paramMembers> members;
                    public int face;
                    public string memo;
                    public string markname;
                    public int level;

                    public class paramMembers
                    {
                        public string muin;
                        public int mflag;
                    }
                }
            }
        }




        public class JsonGroupInfoZD
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                public List<paramStats> stats;
                public List<paramMinfo> minfo;
                // public List<paramCards> cards;
                public paramGinfo ginfo;
                public List<paramVipinfo> vipinfo;
                public class paramStats
                {
                    public string client_type;
                    public string uin;
                    public string stat;
                }
                public class paramMinfo
                {
                    public string nick;
                    public string province;
                    public string gender;
                    public string uin;
                    public string country;
                    public string city;
                }

                public class paramGinfo
                {
                    public string code;
                    public string createtime;
                    public string flag;
                    public string name;
                    public string gid;
                    public string owner;
                    public List<paramMembers> members;
                    public int face;
                    public string memo;
                    public string markname;
                    public int level;

                    public class paramMembers
                    {
                        public string muin;
                        public int mflag;
                    }
                }
                public class paramVipinfo
                {
                    public string vip_level;
                    public string u;
                    public string is_vip;
                }
            }
        }
        #endregion
        #region QQ信息 汇总
        public class JsonFriendModel
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                /// 分组信息
                public List<paramCategories> categories;
                /// 好友汇总
                public List<paramFriends> friends;
                /// 好友信息
                public List<paramInfo> info;
                /// 备注
                public List<paramMarkNames> marknames;
                /// 备注
                public List<paramVipinfo> vipinfo;
                /// 分组
                public class paramCategories
                {
                    public int index;
                    public int sort;
                    public string name;
                }
                /// 好友汇总 
                public class paramFriends
                {
                    public int flag;
                    public string uin;
                    public int categories;
                }
                /// 好友信息
                public class paramInfo
                {
                    public int face;
                    public string nick;
                    public string uin;
                }
                /// 备注 
                public class paramMarkNames
                {
                    public string uin;
                    public string markname;
                }
                /// vip信息
                public class paramVipinfo
                {
                    public int vip_level;
                    public string u;
                    public int is_vip;
                }
            }
        }
        #endregion
        #region 讨论组消息
        /// <summary>
        /// 讨论组消息
        /// </summary>
        public class JsonDisscussModel
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                public List<paramGnamelist> dnamelist;
                public class paramGnamelist
                {
                    public string did;
                    public string name;
                }
            }
        }
        #endregion
        #region 获取讨论组详细信息
        /// <summary>
        /// 获取讨论组详细信息
        /// </summary>
        public class JsonDisscussInfoModel
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                public paramInfo info;
                public List<paramMembeiInfo> mem_info;
                public List<paramMembeiStatus> mem_status;
                public class paramInfo
                {
                    public string discu_owner;
                    public string discu_name;
                    public string did;
                }
                public class paramMembeiInfo
                {
                    public string uin;
                    public string nick;
                }
                public class paramMembeiStatus
                {
                    public string uin;
                    public string status;
                    public int client_type;
                }
            }
        }
        #endregion
        #region 好友详细信息
        /// <summary>
        /// 好友详细信息
        /// </summary>
        public class JsonFriendInfModel
        {
            public int retcode;
            public paramResult result;
            public class paramResult
            {
                public int face;
                public paramBirthday birthday;
                public string occupation;
                /// <summary>
                /// 电话
                /// </summary>
                public string phone;
                /// <summary>
                /// 毕业院校
                /// </summary>
                public string college;
                public int constel;
                public int blood;
                public string homepage;
                public int stat;
                /// <summary>
                /// 城市
                /// </summary>
                public string city;
                public string personal;
                /// <summary>
                /// 昵称
                /// </summary>
                public string nick;
                /// <summary>
                /// 生肖
                /// </summary>
                public int shengxiao;
                /// <summary>
                /// 邮箱
                /// </summary>
                public string email;
                public string province;
                public string gender;
                public string mobile;
                public string country;
                public int vip_info;

                public class paramBirthday
                {
                    public int month;
                    public int year;
                    public int day;
                }
            }
        }
        #endregion
        #region 状态信息
        public class JsonPollMessage
        {
            /// <summary>
            /// 状态码
            /// </summary>
            public int retcode;     //状态码
                                    /// <summary>
                                    /// 错误信息
                                    /// </summary>
            public string errmsg;   //错误信息
                                    /// <summary>
                                    /// 被迫下线说明
                                    /// </summary>
            public string t;        //被迫下线说明
                                    /// <summary>
                                    /// 需要更换的ptwebqq
                                    /// </summary>
            public string p;        //需要更换的ptwebqq
            public List<paramResult> result;

            public class paramResult
            {
                public string poll_type;
                public paramValue value;
                public class paramValue
                {
                    /// <summary>
                    /// 收到消息内容
                    /// </summary>
                    public List<object> content;
                    public string from_uin;
                    /// <summary>
                    /// 收到消息时间
                    /// </summary>
                    public string time;
                    /// <summary>
                    /// 群消息有send_uin，为特征群号；info_seq为群号
                    /// </summary>
                    public string send_uin;
                    //public string info_seq;              
                    //上线提示
                    //public string uin;
                    //public string status;
                    /// <summary>
                    /// 异地登录
                    /// </summary>
                    public string reason;
                    //临时会话
                    //public string id;
                    //public string ruin;
                    //public string service_type;
                    /// <summary>
                    /// 讨论组
                    /// </summary>
                    public string did;
                }
            }
        }
        #endregion
    }
}
