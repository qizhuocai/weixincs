﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK
{
    /// <summary>
    /// 日志记录委托函数
    /// </summary>
    /// <param name="_log">日志内容</param>
    public delegate void SendLogEvent(string _log);

    /// <summary>
    /// 二维码获取成功后的委托
    /// </summary>
    /// <param name="_image">二维码图像</param>
    public delegate void SendQrCodeImageEvent(Image _image);
}
