﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.Log
{
   public static  class SysLog
    {
        /// <summary>
        /// 日志记录委托函数
        /// </summary>
        /// <param name="_log">日志内容</param>
        public delegate void SysSendLog(string _log);

        public static SysSendLog LogEvent = null;

        public delegate void SysSendMsg(string _msg);

        public static SysSendMsg SendMsg = null;


         /// <summary>
        /// 发送信息
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="touser"></param>
        public static  void SendMseege(string msg, string touser)
        {
            WXWebAPI ap=new WXWebAPI ();
            if (touser != null)
            {
                ap.SendMsg(msg, touser);
            }
            else
            {
                Console.Write(msg);
            }
        }
        /// <summary>
        /// 写日志
        /// </summary>
        /// <param name="_info">要记录的日志消息</param>
        public static void SendSysLog(string _info)
        {
            if (LogEvent != null)
            {
                LogEvent(_info);
            }
            else
            {
                Console.Write(_info);
            }
        }
    }
}
