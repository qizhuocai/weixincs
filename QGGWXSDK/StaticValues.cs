﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK
{
    public class StaticValues
    {
        /// <summary>
        /// 微信开发者的appkey  wxe18c5c483ace0130
        /// </summary>
        // public static string uuid = "wxe18c5c483ace0130";
       public static string uuid = "wx782c26e4c19acffb";
        /// <summary>
        /// 系统微信用户名
        /// </summary>
        public static string[] special_users = new string[]{"newsapp", "fmessage", "filehelper", "weibo", "qqmail",
                         "fmessage", "tmessage", "qmessage", "qqsync", "floatbottle",
                         "lbsapp", "shakeapp", "medianote", "qqfriend", "readerapp",
                         "blogapp", "facebookapp", "masssendapp", "meishiapp",
                         "feedsapp", "voip", "blogappweixin", "weixin", "brandsessionholder",
                         "weixinreminder", "wxid_novlwrv3lqwv11", "gh_22b87fa7cb3c",
                         "officialaccounts", "notification_messages", "wxid_novlwrv3lqwv11",
                         "gh_22b87fa7cb3c", "wxitil", "userexperience_alarm", "notification_messages"};
    }
}
