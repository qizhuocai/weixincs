﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.WXObject
{
    /// <summary>
    /// 获取群聊用户信息返回的对象
    /// </summary>
    public class WebWXGroupList
    {
        public BaseResponse BaseResponse { get; set; }

        public long Count { get; set; }

        public List<MemberObj> ContactList { get; set; }
    }


    /// <summary>
    /// 获取群聊用户信息返回的对象
    /// </summary>
    public class WebWXGroupuserList
    {
        public BaseResponse BaseResponse { get; set; }

        public long Count { get; set; }

        public List<GroupChatUser> ContactList { get; set; }
    }
}
