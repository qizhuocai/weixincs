﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.WXObject
{

    public class BaseResponse
    {
        /// <summary>
        /// Ret
        /// </summary>
        public long Ret { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ErrMsg { get; set; }
        /// <summary>
        /// 全部信息字典
        /// </summary>
        public static Dictionary<string, MemberObj> MemberObjList = new Dictionary<string, MemberObj>();
    }
    
    public class MemberBase
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(DisplayName) == false)
            {
                return DisplayName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// @e2fd3cab73debd0364179e86fa2ff114
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// MemberStatus
        /// </summary>
        public long MemberStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// zha
        /// </summary>
        public string KeyWord { get; set; }
    }
    public class MemberBaseuser
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(DisplayName) == false)
            {
                return DisplayName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// @e2fd3cab73debd0364179e86fa2ff114
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// MemberStatus
        /// </summary>
        public long MemberStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// zha
        /// </summary>
        public string KeyWord { get; set; }
    }
    //全部信息
    public class MemberObj
    {
        public string GetDisName()
        {
            if(string.IsNullOrEmpty(RemarkName)==false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin 0
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// 名称@5a0bf58bcf2f7ceb6386ec84f8f81979afabe48779a599ce06b040849033085
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// （文件传输助手） 微信名字 阿柒
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag 1
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount 0
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList 【】
        /// </summary>
        public List<MemberBase> MemberList { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag 0
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// 性别 0,1
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag 0 
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// 好友信息
    /// </summary>
    public class MemberUser
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberBase> MemberList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// MemUser  字典
    /// </summary>
    public class MemUser
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin;
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName;
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName;
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl;
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag;
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount;
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberUser> MemberList;
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName;
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag;
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex;
        /// <summary>
        /// 
        /// </summary>
        public string Signature;
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag;
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin;
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial;
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin;
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial;
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin;
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend;
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag;
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues;
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus;
        /// <summary>
        /// 
        /// </summary>
        public string Province;
        /// <summary>
        /// 
        /// </summary>
        public string City;
        /// <summary>
        /// 
        /// </summary>
        public string Alias;
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag;
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend;
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName;
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId;
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord;
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId;
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner;
    }
    /// <summary>
    /// 公众号信息
    /// </summary>
    public class MemberGroup
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberUser> MemberList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// 个人信息
    /// </summary>
    public class Userer
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberBase> MemberList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// 群聊信息
    /// </summary>
    public class GroupChat
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberBase> MemberList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// 群员信息
    /// </summary>
    public class Groupuser
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberBase> MemberList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// 全部信息
    /// </summary>
    public class GroupChatUser
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin 0
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// 名称@5a0bf58bcf2f7ceb6386ec84f8f81979afabe48779a599ce06b040849033085
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// （文件传输助手） 微信名字 阿柒
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag 1
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount 0
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList 【】
        /// </summary>
        public List<MemberBaseuser> MemberList { get; set; }
        /// <summary>
        /// 备注
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag 0
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// 性别 0,1
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 签名
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag 0 
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 省份
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 城市
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }
    /// <summary>
    /// 其他应用信息
    /// </summary>
    public class OtherMem
    {
        public string GetDisName()
        {
            if (string.IsNullOrEmpty(RemarkName) == false)
            {
                return RemarkName;
            }
            if (string.IsNullOrEmpty(NickName) == false)
            {
                return NickName;
            }
            return UserName;
        }
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// filehelper
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 文件传输助手
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=0&username=filehelper&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberBase> MemberList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// OwnerUin
        /// </summary>
        public long OwnerUin { get; set; }
        /// <summary>
        /// WJCSZS
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// wenjianchuanshuzhushou
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// UniFriend
        /// </summary>
        public long UniFriend { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// ChatRoomId
        /// </summary>
        public long ChatRoomId { get; set; }
        /// <summary>
        /// fil
        /// </summary>
        public string KeyWord { get; set; }
        /// <summary>
        /// EncryChatRoomId
        /// </summary>
        public string EncryChatRoomId { get; set; }
        /// <summary>
        /// IsOwner
        /// </summary>
        public long IsOwner { get; set; }
    }

    public class SyncKeyList
    {
        /// <summary>
        /// Key
        /// </summary>
        public long Key { get; set; }
        /// <summary>
        /// Val
        /// </summary>
        public long Val { get; set; }
    }

    public class SyncKey
    {
        /// <summary>
        /// Count
        /// </summary>
        public long Count { get; set; }
        /// <summary>
        /// List
        /// </summary>
        public List<SyncKeyList> List { get; set; }
    }
    /// <summary>
    ///个人信息
    /// </summary>
    public class User
    {
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// @e64a6cfb94bb585e7c4ac8870d0da6257ef79d49a4124c406f517fb1268c345d
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 啦啦啦
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgeticon?seq=312468352&username=@e64a6cfb94bb585e7c4ac8870d0da6257ef79d49a4124c406f517fb1268c345d&skey=@crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// StarFriend
        /// </summary>
        public long StarFriend { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// AppAccountFlag
        /// </summary>
        public long AppAccountFlag { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// WebWxPluginSwitch
        /// </summary>
        public long WebWxPluginSwitch { get; set; }
        /// <summary>
        /// HeadImgFlag
        /// </summary>
        public long HeadImgFlag { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
    }
    
    /// <summary>
    /// 网页登录微信成功后返回的对象
    /// </summary>
    public class WXInitRoot
    {
        /// <summary>
        /// BaseResponse
        /// </summary>
        public BaseResponse BaseResponse { get; set; }
        /// <summary>
        /// Count
        /// </summary>
        public long Count { get; set; }
        /// <summary>
        /// ContactList
        /// </summary>
        public List<MemberObj> ContactList { get; set; }
        /// <summary>
        /// SyncKey
        /// </summary>
        public SyncKey SyncKey { get; set; }
        /// <summary>
        /// User
        /// </summary>
        public User User { get; set; }
        /// <summary>
        /// filehelper,weixin,
        /// </summary>
        public string ChatSet { get; set; }
        /// <summary>
        /// @crypt_fe6bb81e_02604a7caf94b8167a58dcbf17d32356
        /// </summary>
        public string SKey { get; set; }
        /// <summary>
        /// ClientVersion
        /// </summary>
        public long ClientVersion { get; set; }
        /// <summary>
        /// SystemTime
        /// </summary>
        public long SystemTime { get; set; }
        /// <summary>
        /// GrayScale
        /// </summary>
        public long GrayScale { get; set; }
        /// <summary>
        /// InviteStartCount
        /// </summary>
        public long InviteStartCount { get; set; }
        /// <summary>
        /// MPSubscribeMsgCount
        /// </summary>
        public long MPSubscribeMsgCount { get; set; }
        /// <summary>
        /// MPSubscribeMsgList
        /// </summary>
        public List<Object> MPSubscribeMsgList { get; set; }
        /// <summary>
        /// ClickReportInterval
        /// </summary>
        public long ClickReportInterval { get; set; }
    }
}
