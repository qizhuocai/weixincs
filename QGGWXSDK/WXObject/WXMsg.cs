﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.WXObject
{
    /// <summary>
    /// 本地保存的微信消息记录实体
    /// </summary>
    public class WXMsg
    {
        /// <summary>
        /// 消息发送者，群或者用户
        /// </summary>
        public string From
        {
            get;
            set;
        }
        /// <summary>
        /// 如果是群，此处为群中用户名
        /// </summary>
        public string From2
        {
            get;
            set;
        }

        /// <summary>
        /// 消息发送时间
        /// </summary>
        public DateTime Time
        {
            get;
            set;
        }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Msg
        {
            get;
            set;
        }
        /// <summary>
        /// 某些连接地址
        /// </summary>
        public string Url
        {
            get;
            set;
        }
        /// <summary>
        /// 消息类型
        /// </summary>
        public long Type
        {
            get;
            set;
        }
    }

    public class WXMsgg
    {
        public string Sid { get; set; }
        public string Uin { get; set; }

        /// <summary>
        /// 消息发送方
        /// </summary>
       public string From
        {
            get;
            set;
        }
        /// <summary>
        /// 消息接收方
        /// </summary>
        public string To
        {
            set;
            get;
        }

        /// <summary>
        /// 消息发送时间
        /// </summary>
        public DateTime Time
        {
            get;
            set;
        }
        /// <summary>
        /// 是否已读
        /// </summary>
        public bool Readed
        {
            get;
            set;
        }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Msg
        {
            get;
            set;
        }
        /// <summary>
        /// 消息类型 1:文本消息  51系统消息  10000 进群退群消息
        /// </summary>
        public int Type
        {
            get;
            set;
        }
    }

    public class SendMsg
    {
        public string context { get; set; }
        public int type { get; set; }
    }

    public class NotifyArgs : EventArgs
    {
        public string MsgContext { get; set; }
        public string WxUin { get; set; }
        public string Sid { get; set; }
        public string MyUserName { get; set; }
        public List<string> GroupUserName { get; set; }
    }
}
