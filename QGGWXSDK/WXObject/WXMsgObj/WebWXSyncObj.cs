﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.WXObject.WXMsgObj
{
    public class BaseResponse
    {
        /// <summary>
        /// Ret
        /// </summary>
        public long Ret { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ErrMsg { get; set; }
    }

    public class RecommendInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// QQNum
        /// </summary>
        public long QQNum { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// Scene
        /// </summary>
        public long Scene { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Ticket { get; set; }
        /// <summary>
        /// OpCode
        /// </summary>
        public long OpCode { get; set; }
    }

    public class AppInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string AppID { get; set; }
        /// <summary>
        /// Type
        /// </summary>
        public long Type { get; set; }
    }

    public class AddMsgList
    {
        /// <summary>
        /// 1475989468359185202
        /// </summary>
        public string MsgId { get; set; }
        /// <summary>
        /// @5e41e1dcbb4141b0ad7f4f8bf4205c39877f54d467c130b797eab9409eeac801
        /// </summary>
        public string FromUserName { get; set; }
        /// <summary>
        /// @5e41e1dcbb4141b0ad7f4f8bf4205c39877f54d467c130b797eab9409eeac801
        /// </summary>
        public string ToUserName { get; set; }
        /// <summary>
        /// MsgType
        /// </summary>
        public long MsgType { get; set; }
        /// <summary>
        /// &lt;msg&gt;<br/>&lt;op id='4'&gt;<br/>&lt;username&gt;filehelper,qqmail,6971142380@chatroom,zhanggangbz,weixin&lt;/username&gt;<br/>&lt;unreadchatlist&gt;<br/>&lt;/unreadchatlist&gt;<br/>&lt;unreadfunctionlist&gt;<br/>&lt;/unreadfunctionlist&gt;<br/>&lt;/op&gt;<br/>&lt;/msg&gt;
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public long Status { get; set; }
        /// <summary>
        /// ImgStatus
        /// </summary>
        public long ImgStatus { get; set; }
        /// <summary>
        /// CreateTime
        /// </summary>
        public long CreateTime { get; set; }
        /// <summary>
        /// VoiceLength
        /// </summary>
        public long VoiceLength { get; set; }
        /// <summary>
        /// PlayLength
        /// </summary>
        public long PlayLength { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FileName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FileSize { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MediaId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// AppMsgType
        /// </summary>
        public long AppMsgType { get; set; }
        /// <summary>
        /// StatusNotifyCode
        /// </summary>
        public long StatusNotifyCode { get; set; }
        /// <summary>
        /// filehelper,qqmail,@@e5213d3a07afc526848ed469af7c034ffa0dec3eac6acfa6a4e47c75deb4ed3e,@cf9c92faef7c6398c8c61bcaebcb0815,weixin
        /// </summary>
        public string StatusNotifyUserName { get; set; }
        /// <summary>
        /// RecommendInfo
        /// </summary>
        public RecommendInfo RecommendInfo { get; set; }
        /// <summary>
        /// ForwardFlag
        /// </summary>
        public long ForwardFlag { get; set; }
        /// <summary>
        /// AppInfo
        /// </summary>
        public AppInfo AppInfo { get; set; }
        /// <summary>
        /// HasProductId
        /// </summary>
        public long HasProductId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Ticket { get; set; }
        /// <summary>
        /// ImgHeight
        /// </summary>
        public long ImgHeight { get; set; }
        /// <summary>
        /// ImgWidth
        /// </summary>
        public long ImgWidth { get; set; }
        /// <summary>
        /// SubMsgType
        /// </summary>
        public long SubMsgType { get; set; }
        /// <summary>
        /// NewMsgId
        /// </summary>
        public long NewMsgId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OriContent { get; set; }
    }

    public class MemberList
    {
        /// <summary>
        /// Uin
        /// </summary>
        public long Uin { get; set; }
        /// <summary>
        /// @cf9c92faef7c6398c8c61bcaebcb0815
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 刚刚
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PYQuanPin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYInitial { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkPYQuanPin { get; set; }
        /// <summary>
        /// MemberStatus
        /// </summary>
        public long MemberStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DisplayName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string KeyWord { get; set; }
    }

    public class ModContactList
    {
        /// <summary>
        /// @@e5213d3a07afc526848ed469af7c034ffa0dec3eac6acfa6a4e47c75deb4ed3e
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string NickName { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// HeadImgUpdateFlag
        /// </summary>
        public long HeadImgUpdateFlag { get; set; }
        /// <summary>
        /// ContactType
        /// </summary>
        public long ContactType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// @cf9c92faef7c6398c8c61bcaebcb0815
        /// </summary>
        public string ChatRoomOwner { get; set; }
        /// <summary>
        /// /cgi-bin/mmwebwx-bin/webwxgetheadimg?seq=0&username=@@e5213d3a07afc526848ed469af7c034ffa0dec3eac6acfa6a4e47c75deb4ed3e&skey=@crypt_fe6bb81e_4d908bea44a0d5b9e422f794e94094a8
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// ContactFlag
        /// </summary>
        public long ContactFlag { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public long MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberList> MemberList { get; set; }
        /// <summary>
        /// HideInputBarFlag
        /// </summary>
        public long HideInputBarFlag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
        /// <summary>
        /// VerifyFlag
        /// </summary>
        public long VerifyFlag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RemarkName { get; set; }
        /// <summary>
        /// Statues
        /// </summary>
        public long Statues { get; set; }
        /// <summary>
        /// AttrStatus
        /// </summary>
        public long AttrStatus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Province { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string City { get; set; }
        /// <summary>
        /// SnsFlag
        /// </summary>
        public long SnsFlag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string KeyWord { get; set; }
    }

    public class UserName
    {
        /// <summary>
        /// 
        /// </summary>
        public string Buff { get; set; }
    }

    public class NickName
    {
        /// <summary>
        /// 
        /// </summary>
        public string Buff { get; set; }
    }

    public class BindEmail
    {
        /// <summary>
        /// 
        /// </summary>
        public string Buff { get; set; }
    }

    public class BindMobile
    {
        /// <summary>
        /// 
        /// </summary>
        public string Buff { get; set; }
    }

    public class Profile
    {
        /// <summary>
        /// BitFlag
        /// </summary>
        public long BitFlag { get; set; }
        /// <summary>
        /// UserName
        /// </summary>
        public UserName UserName { get; set; }
        /// <summary>
        /// NickName
        /// </summary>
        public NickName NickName { get; set; }
        /// <summary>
        /// BindUin
        /// </summary>
        public long BindUin { get; set; }
        /// <summary>
        /// BindEmail
        /// </summary>
        public BindEmail BindEmail { get; set; }
        /// <summary>
        /// BindMobile
        /// </summary>
        public BindMobile BindMobile { get; set; }
        /// <summary>
        /// Status
        /// </summary>
        public long Status { get; set; }
        /// <summary>
        /// Sex
        /// </summary>
        public long Sex { get; set; }
        /// <summary>
        /// PersonalCard
        /// </summary>
        public long PersonalCard { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Alias { get; set; }
        /// <summary>
        /// HeadImgUpdateFlag
        /// </summary>
        public long HeadImgUpdateFlag { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Signature { get; set; }
    }

    public class WebWXSyncObj
    {
        /// <summary>
        /// BaseResponse
        /// </summary>
        public BaseResponse BaseResponse { get; set; }
        /// <summary>
        /// AddMsgCount
        /// </summary>
        public long AddMsgCount { get; set; }
        /// <summary>
        /// AddMsgList
        /// </summary>
        public List<AddMsgList> AddMsgList { get; set; }
        /// <summary>
        /// ModContactCount
        /// </summary>
        public long ModContactCount { get; set; }
        /// <summary>
        /// ModContactList
        /// </summary>
        public List<ModContactList> ModContactList { get; set; }
        /// <summary>
        /// DelContactCount
        /// </summary>
        public long DelContactCount { get; set; }
        /// <summary>
        /// DelContactList
        /// </summary>
        public List<ModContactList> DelContactList { get; set; }
        /// <summary>
        /// ModChatRoomMemberCount
        /// </summary>
        public long ModChatRoomMemberCount { get; set; }
        /// <summary>
        /// ModChatRoomMemberList
        /// </summary>
        public List<ModContactList> ModChatRoomMemberList { get; set; }
        /// <summary>
        /// Profile
        /// </summary>
        public Profile Profile { get; set; }
        /// <summary>
        /// ContinueFlag
        /// </summary>
        public long ContinueFlag { get; set; }
        /// <summary>
        /// SyncKey
        /// </summary>
        public SyncKey SyncKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SKey { get; set; }
        /// <summary>
        /// SyncCheckKey
        /// </summary>
        public SyncKey SyncCheckKey { get; set; }
    }
}
