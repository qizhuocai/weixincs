﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.WXObject
{
    public class SvaeWXMsgObj
    {
        public static Dictionary<string, string> AllAutoMsg = new Dictionary<string, string>();
        
        /// <summary>
        /// 自定义的消息保存函数
        /// </summary>
        /// <param name="_curMsg"></param>
        public static void DoSave(WXMsg _curMsg)
        {
            ///保存消息到本地的sqlite库
            saveToSqlite(_curMsg);
        }

        /// <summary>
        /// 保存当前所给的消息到sqlite数据库中
        /// </summary>
        /// <param name="_curMsg"></param>
        private static void saveToSqlite(WXMsg _curMsg)
        {
            //string sql = "CREATE TABLE IF NOT EXISTS wxmsg (MSG_ID  TEXT NOT NULL,MSG_FROM1  TEXT,MSG_FROM2  TEXT,MSG_CONTENT  TEXT,MSG_TYPE  INTEGER,MSG_TIME  TEXT,PRIMARY KEY (MSG_ID));";//建表语句  
            //SQLiteCommand cmdCreateTable = new SQLiteCommand(sql, conn);

            SQLiteDBHelper db = new SQLiteDBHelper(Environment.CurrentDirectory + "/wxbot.db");

            string sql = "INSERT INTO wxmsg(MSG_ID, MSG_FROM1, MSG_FROM2, MSG_CONTENT, MSG_TYPE, MSG_TIME, MSG_URL) VALUES (@id,@from1,@from2,@content,@type,@time,@url)";

            SQLiteParameter[] parameters = new SQLiteParameter[]{ 
                                                 new SQLiteParameter("@id",Guid.NewGuid().ToString("N")), 
                                         new SQLiteParameter("@from1",_curMsg.From), 
                                         new SQLiteParameter("@from2",_curMsg.From2), 
                                         new SQLiteParameter("@content",_curMsg.Msg), 
                                         new SQLiteParameter("@type",_curMsg.Type), 
                                         new SQLiteParameter("@url",_curMsg.Url), 
                                         new SQLiteParameter("@time",_curMsg.Time.ToString("yyyy-MM-dd HH:mm:ss.ffff")) 
                                         };
            db.ExecuteNonQuery(sql, parameters);
        }

        /// <summary>
        /// 从数据库中获取所有的自动回复消息
        /// </summary>
        public static void GetAllAutoMsg()
        {
            string sql = "select * from autoMsg";

            SQLiteDBHelper db = new SQLiteDBHelper(Environment.CurrentDirectory + "/wxbot.db");

            using (SQLiteDataReader reader = db.ExecuteReader(sql, null))
            {
                while (reader.Read())
                {
                    if (!AllAutoMsg.ContainsKey(reader.GetString(0)))
                    {
                        AllAutoMsg.Add(reader.GetString(0), reader.GetString(1));

                    }
               
                }
            } 
        }

        /// <summary>
        /// 向数据库增加自动回复消息
        /// </summary>
        /// <param name="remsg">接收到的消息</param>
        /// <param name="sendmsg">要回复的消息</param>
        public static void InsertOneAutoMsg(string remsg,string sendmsg)
        {
            SQLiteDBHelper db = new SQLiteDBHelper(Environment.CurrentDirectory + "/wxbot.db");

            string sql = "INSERT INTO autoMsg(RE_MSG,SEND_MSG) VALUES (@re,@send)";

            SQLiteParameter[] parameters = new SQLiteParameter[]{ 
                new SQLiteParameter("@re",remsg), 
                new SQLiteParameter("@send",sendmsg) 
            };
            db.ExecuteNonQuery(sql, parameters);
        }
    }
}
