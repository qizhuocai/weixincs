﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGGWXSDK.WXObject
{
    /// <summary>
    /// 获取所有用户返回的对象
    /// </summary>
    public class WebWXMemberObject
    {
        public BaseResponse BaseResponse { get; set; }
        /// <summary>
        /// MemberCount
        /// </summary>
        public int MemberCount { get; set; }
        /// <summary>
        /// MemberList
        /// </summary>
        public List<MemberObj> MemberList { get; set; }
        /// <summary>
        /// 普通用户列表
        /// </summary>
        public List<MemberUser> MemberListUser { get; set; }
        /// <summary>
        /// 公众号列表
        /// </summary>
        public List<MemberGroup> MemberListGroup { get; set; }
        /// <summary>
        /// 群聊列表
        /// </summary>
        public List<GroupChat> GroupChatList { get; set; }
        /// <summary>
        /// 个人列表
        /// </summary>
        public List<Userer> UsererList { get; set; }
        /// <summary>
        /// Seq
        /// </summary>
        public int Seq { get; set; }

        
        /// <summary>
        /// 普通成员
        /// </summary>
        public List<MemberObj> NormalMember = new List<MemberObj>();

        /// <summary>
        /// 公众号
        /// </summary>
        public List<MemberObj> PublicMember = new List<MemberObj>();
      
        /// <summary>
        /// 群聊
        /// </summary>
        public List<MemberObj> GroupMember = new List<MemberObj>();
        /// <summary>
        /// 其他用户
        /// </summary>
        public List<MemberObj> OtherMember = new List<MemberObj>();
    
        ///// <summary>
        ///// 群聊
        ///// </summary>
        //public List<GroupChat> GroupMember = new List<GroupChat>();
        ///// <summary>
        ///// 个人
        ///// </summary>
        //public List<Userer> OtherMember = new List<Userer>();
        ///// <summary>
        ///// 公众号
        ///// </summary>
        //public List<MemberGroup> PublicMember = new List<MemberGroup>();
        ///// <summary>
        ///// 普通成员
        ///// </summary>
        //public List<MemberUser> NormalMember = new List<MemberUser>();

        /// <summary>
        /// 对用户进行分类
        /// </summary>
        public void SortMembers()
        {
            if(MemberList != null)
            {
                foreach (MemberObj oneMem in MemberList)
                {
                    if (((int)oneMem.VerifyFlag & 8) != 0)
                    {

                        PublicMember.Add(oneMem);
                    }
                    else if (StaticValues.special_users.Contains(oneMem.UserName))
                    {

                        OtherMember.Add(oneMem);
                      

                    }
                    else if (oneMem.UserName.IndexOf("@@") != -1)
                    {

                        GroupMember.Add(oneMem);

                    }
                    else
                    {

                        NormalMember.Add(oneMem);
                        
                    }
                }
            }
        }
    }
}
