﻿using Newtonsoft.Json;
using System;
using System.Drawing;
using System.Net;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using System.Linq;
using System.Data;
using System.Collections;

namespace QGWebQQSDK
{
    public static class SmartQQ
    {
      
        public enum LoginStatus { Valid, Invalid, Checking, LoginSuccess, LoginFailed };
        private static System.Timers.Timer Login_QRStatuTimer = new System.Timers.Timer();
        public  static string vfwebqq, ptwebqq, psessionid, uin, hash;
      
        /// <summary>
        /// 消息委托
        /// </summary>
        /// 
        public delegate void DelegateReceivelog(string log);
        public delegate void DelegateReceiveLoginStatus(LoginStatus status);
    
        public  static  event DelegateReceiveLoginStatus EventReceiveLoginStatus;
 
        public static  Form formObject;
        /// <summary>
        /// 消息事件对象
        /// </summary>
        public static  event DelegateReceivelog EventReceivelog;
   
        /// <summary>
        /// QQ号码
        /// </summary>
        public static string QQNum;
        private static Random rand = new Random();
        public static FriendInfo SelfInfo = new FriendInfo();
        public static Dictionary<string, FriendInfo> FriendList = new Dictionary<string, FriendInfo>();
        public static Dictionary<string, GroupInfo> GroupList = new Dictionary<string, GroupInfo>();
        public static Dictionary<string, ZDInfo> ZDInfoList = new Dictionary<string, ZDInfo>();
        public static Dictionary<string, DiscussInfo> DiscussList = new Dictionary<string, DiscussInfo>();
        public static Dictionary<string, string> RealQQNum = new Dictionary<string, string>();
        public static Dictionary<string, string> RealQQuin = new Dictionary<string, string>();
        public static string[] FriendCategories = new string[100];
        public static event EventHandler< QGWebQQSDK.Information.GroupMessage> GroupMessage = null;
        static int ptqrtoken = 0;

        #region 开始登录SmartQQ
        /// <summary>
        /// 开始登录SmartQQ
        /// </summary>
        public static void Login()
        {
            //Login_GetQRCode();
            //Program.MainForm.buttonLogIn.Enabled = false;
           Login_QRStatuTimer.AutoReset = true;
           Login_QRStatuTimer.Elapsed += Login_QRStatuTimer_Elapsed;
           Login_QRStatuTimer.Interval = 1000;
            Login_QRStatuTimer.Start();
        }
        #endregion

        #region 登录界面
        #region 登录第一步：获取登陆用二维码
        /// <summary>
        /// 登录第一步：获取登陆用二维码
        /// </summary>
        /// <returns>成功：true</returns>
        public static Image Login_GetQRCode()
        {

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create("https://ssl.ptlogin2.qq.com/ptqrshow?appid=501004106&e=2&l=M&s=3&d=72&v=4&t=#{t}&daid=164&pt_3rd_aid=0".Replace("#{t}", rand.NextDouble().ToString()));
            req.CookieContainer = HTTP.cookies;
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            ptqrtoken = Hash33(GetAllCookies(res.Cookies));
            return Image.FromStream(res.GetResponseStream());

        }
        public static int Hash33(string s)
        {
            var e = 0;
            foreach (var t in s)
                e += (e << 5) + t;
            return int.MaxValue & e;
        }
        public static string GetAllCookies(CookieCollection cc)
        {
            //List<Cookie> lstCookies = new List<Cookie>();
            string lstCookies = "";
            ArrayList table = (ArrayList)cc.GetType().InvokeMember("m_list",
                System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.GetField |
                System.Reflection.BindingFlags.Instance, null, cc, new object[] { });

            if (table != null)
            {
                for (int i = 0; i < table.Count; i++)
                {
                    string[] tacookie = table[i].ToString().Split('=');
                    lstCookies = tacookie[1].ToString().Trim();
                }
            }


            return lstCookies;
        }
        #endregion
        #region 每秒检查一次二维码状态
        /// <summary>
        /// 每秒检查一次二维码状态
        /// </summary>
        private static void Login_QRStatuTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Login_GetQRStatu();
        }
        #endregion
        #region 登录第二步：检查二维码状态
        /// <summary>
        /// 登录第二步：检查二维码状态
        /// </summary>
        public  static void Login_GetQRStatu()
        {
            string dat;
            //dat = HTTP.Get("https://ssl.ptlogin2.qq.com/ptqrlogin?webqq_type=10&remember_uin=1&login2qq=1&aid=501004106 &u1=http%3A%2F%2Fw.qq.com%2Fproxy.html%3Flogin2qq%3D1%26webqq_type%3D10 &ptredirect=0&ptlang=2052&daid=164&from_ui=1&pttype=1&dumy=&fp=loginerroralert &action=0-0-157510&mibao_css=m_webqq&t=1&g=1&js_type=0&js_ver=10143&login_sig=&pt_randsalt=0", "https://ui.ptlogin2.qq.com/cgi-bin/login?daid=164&target=self&style=16&mibao_css=m_webqq&appid=501004106&enable_qlogin=0&no_verifyimg=1 &s_url=http%3A%2F%2Fw.qq.com%2Fproxy.html&f_url=loginerroralert &strong_login=1&login_state=10&t=20131024001");
            dat = HTTP.Get(string.Format("https://ssl.ptlogin2.qq.com/ptqrlogin?ptqrtoken={0}&webqq_type=10&remember_uin=1&login2qq=1&aid=501004106&u1=https%3A%2F%2Fw.qq.com%2Fproxy.html%3Flogin2qq%3D1%26webqq_type%3D10&ptredirect=0&ptlang=2052&daid=164&from_ui=1&pttype=1&dumy=&fp=loginerroralert&0-0-157510&mibao_css=m_webqq&t=undefined&g=1&js_type=0&js_ver=10184&login_sig=&pt_randsalt=3", ptqrtoken));

            string[] temp = dat.Split('\'');
            switch (temp[1])
            {
                case ("65"):                                            //二维码失效
                    ExecuteEventLoginStatus(LoginStatus.Invalid);
                   // Program.MainForm.labelQRStatu.Text = "当前登录状态：二维码失效，请稍后";
                   // Login_GetQRCode();
                    break;
                case ("66"):                                            //等待扫描
                    ExecuteEventLoginStatus(LoginStatus.Checking);
                  ///  Program.MainForm.labelQRStatu.Text = "当前登录状态：二维码有效，请扫描";
                    break;
                case ("67"):                                            //等待确认
                    ExecuteEventLoginStatus(LoginStatus.Valid);   
                 //   Program.MainForm.labelQRStatu.Text = "当前登录状态：二维码已扫描，请确认";
                    break;
                case ("0"):                                             //已经确认
                    Login_Process(temp[5]);
                    ExecuteEventLoginStatus(LoginStatus.LoginSuccess);
                  
                 //   Program.MainForm.labelQRStatu.Text = "当前登录状态：确认成功，请稍候";
                   // Login_Process(temp[5]);
                    break;

                default: break;
            }

        }
        #endregion
        #region 获取登录二维码 委托
        public static  void ExecuteEventLoginStatus(LoginStatus status)
        {
            if (formObject != null)
            {
                formObject.Invoke(EventReceiveLoginStatus, new object[] { status });
            }

        }
        public static void BindEventLoginStatus(Form form, DelegateReceiveLoginStatus ReceiveLogin)
        {
            formObject = form;
            EventReceiveLoginStatus += ReceiveLogin;
        }
        public static void BindEventLoginStatus(DelegateReceiveLoginStatus ReceiveLogin)
        {
            EventReceiveLoginStatus += ReceiveLogin;
        }
        #endregion
        #region 处理扫描二维码并确认后的登录流程
        /// <summary>
        /// 处理扫描二维码并确认后的登录流程
        /// </summary>
        /// <param name="url">获取ptwebqq的跳转地址</param>
        public  static void Login_Process(string url)
        {
            Login_QRStatuTimer.Stop();
            Login_GetPtwebqq(url);
            Login_GetVfwebqq();
            Login_GetPsessionid();
         //   Program.MainForm.pictureBoxQRCode.Image = ((Image)((new System.ComponentModel.ComponentResourceManager(typeof(MainForm))).GetObject("pictureBoxQRCode.Image")));
           // Sleep(2000);
         SendOnlog("获取好友资料中。。。。。");
            Info_FriendList();
          //  Sleep(3000);
            SendOnlog("获取群信息资料中。。。。。");
            Info_GroupList();
            SendOnlog("获取讨论组资料中。。。。。");
          //  Sleep(1000);
            Info_DisscussList(); 
          //  Sleep(1000);
            Info_SelfInfo();
            Login_GetOnlineAndRecent_FAKE();
            Task.Run(() => Message_Request());

       
        }
        #endregion
        #region 登录第三步：获取ptwebqq值
        /// <summary>
        /// 登录第三步：获取ptwebqq值
        /// </summary>
        /// <param name="url">获取ptwebqq的跳转地址</param>
        private static void Login_GetPtwebqq(string url)
        {
            string dat = HTTP.Get(url, "http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1");
            Uri uri = new Uri("http://web2.qq.com/");
            ptwebqq = HTTP.cookies.GetCookies(uri)["ptwebqq"].Value;
            SendOnlog(" 登录第三步：获取ptwebqq值" + ptwebqq);
        }
        #endregion
        #region 登录第四步：获取vfwebqq的值
        /// <summary>
        /// 登录第四步：获取vfwebqq的值
        /// </summary>
        private static void Login_GetVfwebqq()
        {
            string url = "http://s.web2.qq.com/api/getvfwebqq?ptwebqq=#{ptwebqq}&clientid=53999199&psessionid=&t=#{t}".Replace("#{ptwebqq}", ptwebqq).Replace("#{t}", AID_TimeStamp());
            string dat = HTTP.Get(url, "http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1");
            vfwebqq = dat.Split('\"')[7];
            SendOnlog("登录第四步：获取vfwebqq的值" + vfwebqq);
        }
        #endregion
        #region 登录第五步：获取pessionid
        /// <summary>
        /// 登录第五步：获取pessionid
        /// </summary>
        private static void Login_GetPsessionid()
        {
            string url = "http://d1.web2.qq.com/channel/login2";
            string url1 = "{\"ptwebqq\":\"#{ptwebqq}\",\"clientid\":53999199,\"psessionid\":\"\",\"status\":\"online\"}".Replace("#{ptwebqq}", ptwebqq);
            url1 = "r=" + HttpUtility.UrlEncode(url1);
            string dat = HTTP.Post(url, url1, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");
            psessionid = dat.Replace(":", ",").Replace("{", "").Replace("}", "").Replace("\"", "").Split(',')[10];
            QQNum = uin = dat.Replace(":", ",").Replace("{", "").Replace("}", "").Replace("\"", "").Split(',')[14];
            hash = AID_Hash(QQNum, ptwebqq);
            SendOnlog("登录第五步：获取pessionid" + psessionid);
        }
        #endregion
        #region 登录第六步：获取在线成员、近期联系人（仅提交请求，未处理）
        /// <summary>
        /// 登录第六步：获取在线成员、近期联系人（仅提交请求，未处理）
        /// </summary>
        private static void Login_GetOnlineAndRecent_FAKE()
        {
            string url = "http://d1.web2.qq.com/channel/get_online_buddies2?vfwebqq=#{vfwebqq}&clientid=53999199&psessionid=#{psessionid}&t=#{t}".Replace("#{vfwebqq}", vfwebqq).Replace("#{psessionid}", psessionid).Replace("#{t}", AID_TimeStamp());
            HTTP.Get(url, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");

            url = "http://d1.web2.qq.com/channel/get_recent_list2";
            string url1 = "{\"vfwebqq\":\"#{vfwebqq}\",\"clientid\":53999199,\"psessionid\":\"#{psessionid}\"}".Replace("#{vfwebqq}", vfwebqq).Replace("#{psessionid}", psessionid);
            string dat = HTTP.Post(url, "r=" + HttpUtility.UrlEncode(url1), "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");
            SendOnlog("登录第六步：获取在线成员、近期联系人（仅提交请求，未处理）"+dat );
        }
        #endregion
        #endregion

        #region  消息接收处理
        #region 发送poll包，请求消息
        /// <summary>
        /// 发送poll包，请求消息
        /// </summary>
        public  static void Message_Request()
        {
            try
            {
                string url = "http://d1.web2.qq.com/channel/poll2";
                string HeartPackdata = "{\"ptwebqq\":\"#{ptwebqq}\",\"clientid\":53999199,\"psessionid\":\"#{psessionid}\",\"key\":\"\"}";
                HeartPackdata = HeartPackdata.Replace("#{ptwebqq}", ptwebqq).Replace("#{psessionid}", psessionid);
                HeartPackdata = "r=" + HttpUtility.UrlEncode(HeartPackdata);
                HTTP.Post_Async_Action action = Message_Get;
                HTTP.Post_Async(url, HeartPackdata, action);
            }
            catch (Exception) { Message_Request(); }
        }
        #endregion
        #region 接收到消息的回调函数
        /// <summary>
        /// 接收到消息的回调函数
        /// </summary>
        /// <param name="data">接收到的数据（json）</param>
        private static bool Running = true;
        public  static void Message_Get(string data)
        {
            Task.Run(() => Message_Request());
            if (Running)
                Task.Run(() => Message_Process(data));
        }
        #endregion
        #region 处理收到的消息
        /// <summary>
        /// 处理收到的消息
        /// </summary>
        /// <param name="data">收到的消息（JSON）</param>
        public  static void Message_Process(string data)
        {
           // Program.MainForm.textBoxLog.Text = data;
            JsonPollMessage poll = (JsonPollMessage)JsonConvert.DeserializeObject(data, typeof(JsonPollMessage));
            if (poll.retcode != 0)
                Message_Process_Error(poll);
            else if (poll.result != null && poll.result.Count > 0)
                for (int i = 0; i < poll.result.Count; i++)
                {
                    switch (poll.result[i].poll_type)
                    {
                        case "kick_message":
                            Running = false;
                            MessageBox.Show(poll.result[i].value.reason);
                            break;
                        case "message":
                            Message_Process_Message(poll.result[i].value);
                            break;
                        case "group_message":
                            Message_Process_GroupMessage(poll.result[i].value);
                            break;
                        case "discu_message":
                            Message_Process_DisscussMessage(poll.result[i].value);
                            break;
                        default:
                           // Program.MainForm.listBoxLog.Items.Add(poll.result[i].poll_type);
                            break;
                    }
                }
        }
        #endregion
        #region 处理poll包中的消息数组
        /// <summary>
        /// 处理poll包中的消息数组
        /// </summary>
        /// <param name="content">消息数组</param>
        /// <returns></returns>
        private static string Message_Process_GetMessageText(List<object> content)
        {
            string message = "";
            for (int i = 1; i < content.Count; i++)
            {
                if (content[i].ToString().Contains("[\"cface\","))
                    continue;
                else if (content[i].ToString().Contains("\"face\","))
                    message += ("{..[face" + content[i].ToString().Replace("\"face\",", "").Replace("]", "").Replace("[", "").Replace(" ", "").Replace("\r", "").Replace("\n", "") + "]..}");
                else
                    message += content[i].ToString();
            }
            message = message.Replace("\\\\n", Environment.NewLine).Replace("＆", "&");
            return message;
        }
        #endregion
        #region 私聊消息处理
        /// <summary>
        /// 私聊消息处理
        /// </summary>
        /// <param name="value">poll包中的value</param>
        public  static void Message_Process_Message(JsonPollMessage.paramResult.paramValue value)
        {
            string message = Message_Process_GetMessageText(value.content);
            string nick = "未知";
            if (!FriendList.ContainsKey(value.from_uin))
                Info_FriendList();
            if (FriendList.ContainsKey(value.from_uin))
            { nick = FriendList[value.from_uin].nick; }
          //  string qq = Info_RealQQ(value.from_uin);
          /*  QGWebQQSDK.Information.GroupMessage groupMessage=new QGWebQQSDK.Information.GroupMessage ();
           groupMessage.Time  = value.time;
           groupMessage.QQ = Info_RealQQ(value.from_uin);
           groupMessage.Nick = nick;
           groupMessage.Message = message;*/
           Message.MessageManager.GetInstance().Addmessage(Info_RealQQ(value.from_uin), nick, message, value.time);
            SendOnlog(message, 1, nick + "（" + Info_RealQQ(value.from_uin));  
        }
        #endregion
        #region 群聊消息处理
        /// <summary>
        /// 群聊消息处理
        /// </summary>JsonGroupMessage
        /// <param name="value">poll包中的value</param>
        private static void Message_Process_GroupMessage(JsonPollMessage.paramResult.paramValue value)
        {
            string message = Message_Process_GetMessageText(value.content);
            string gid = value.from_uin;
            string gno = AID_GroupKey(gid);
            if (gno.Equals("FAIL"))
                return;
            string nick = "未知";
            if (GroupList[gid].MemberList.ContainsKey(value.send_uin))
                nick = GroupList[gid].MemberList[value.send_uin].nick;
            if (Info_RealQQ(value.send_uin).Equals("1000000"))
                nick = "系统消息";
           // Program.MainForm.AddAndReNewTextBoxGroupChat(value.from_uin, (GroupList[gid].name + "   " + nick + "  " + Info_RealQQ(value.send_uin) + Environment.NewLine + message), false);
        //    SendOnlog("（" + Info_RealQQ(value.send_uin) + "\\" + nick + "*" + Info_RealQQ(value.from_uin) + "\\" + GroupList[gid].name + "）群消息： " + message);           
            SendOnlog(message, 2, nick + "（" + Info_RealQQ(value.send_uin)
                + "）】//【" + GroupList[gid].name + "（" + Info_RealQQ(value.from_uin) + "）");  
            //RuiRui.AnswerGroupMessage(gid, message, value.send_uin, gno);
        }
        #endregion
        #region 讨论组消息处理
        /// <summary>
        /// 讨论组消息处理
        /// </summary>
        /// <param name="value">poll包中的value</param>
        private static void Message_Process_DisscussMessage(JsonPollMessage.paramResult.paramValue value)
        {
            string message = Message_Process_GetMessageText(value.content);
            string DName = "讨论组";
            string SenderNick = "未知";
            if (!DiscussList.ContainsKey(value.did))
                Info_DisscussList();
            if (DiscussList.ContainsKey(value.did))
            {
                DName += DiscussList[value.did].name;
                if (DiscussList[value.did].MemberList.ContainsKey(value.send_uin))
                    SenderNick = DiscussList[value.did].MemberList[value.send_uin].nick;
            }
            else DName = "未知讨论组";
            if (Info_RealQQ(value.send_uin).Equals("1000000"))
                SenderNick = "系统消息";
           // Program.MainForm.AddAndReNewTextBoxDiscussChat(value.from_uin, (DName + "   " + SenderNick + "  " + Info_RealQQ(value.send_uin) + Environment.NewLine + message), false);
            //RuiRui.AnswerMessage(value.did, message, 2);
         //  SysOnlog("收到讨论组消息：" + Info_RealQQ(value.send_uin) + ": "  + message);
            SendOnlog(message, 3, SenderNick + "（" + Info_RealQQ(value.send_uin)
               + "）】//【" + DName + "（" + Info_RealQQ(value.from_uin) + "）");  
        }
        #endregion
        #region 错误信息处理
        /// <summary>
        /// 错误信息处理
        /// </summary>
        /// <param name="poll">poll包</param>
        private static int Count103 = 0;
        private static void Message_Process_Error(JsonPollMessage poll)
        {
            int TempCount103 = Count103;
            Count103 = 0;
            if (poll.retcode == 102)
            {
                return;
            }
            else if (poll.retcode == 103)
            {
                SendOnlog("错误消息：" + "retcode:" + poll.retcode);
               // Program.MainForm.listBoxLog.Items.Insert(0, "retcode:103");
                Count103 = TempCount103 + 1;
                if (Count103 > 20)
                {
                    Running = false;
                    MessageBox.Show("retcode:" + poll.retcode);
                }
                return;
            }
            else if (poll.retcode == 116)
            {
                SendOnlog("错误消息：" + "retcode:" + poll.retcode);
               // Program.MainForm.listBoxLog.Items.Insert(0, "retcode:" + poll.retcode + poll.p);
                ptwebqq = poll.p;
                return;
            }
            else if (poll.retcode == 108 || poll.retcode == 114)
            {
                SendOnlog("错误消息：" + "retcode:" + poll.retcode);
               // Program.MainForm.listBoxLog.Items.Insert(0, "retcode:" + poll.retcode);
                Running = false;
                MessageBox.Show("retcode:" + poll.retcode);
                return;
            }
            else if (poll.retcode == 120 || poll.retcode == 121)
            {
                SendOnlog("错误消息：" + "retcode:" + poll.retcode);
               // Program.MainForm.listBoxLog.Items.Insert(0, "retcode:" + poll.retcode);
               // Program.MainForm.listBoxLog.Items.Insert(0, poll.t);
                Running = false;
                MessageBox.Show("retcode:" + poll.retcode);
                return;
            }
            else if (poll.retcode == 100006 || poll.retcode == 100003)
            {
                SendOnlog("错误消息：" + "retcode:" + poll.retcode);
              //  Program.MainForm.listBoxLog.Items.Insert(0, "retcode:" + poll.retcode);
                Running = false;
                MessageBox.Show("retcode:" + poll.retcode);
                return;
            }
            SendOnlog("错误消息：" + "retcode:" + poll.retcode);
        
           // Program.MainForm.listBoxLog.Items.Insert(0, "retcode:" + poll.retcode);
        }
        #endregion
        #endregion

        #region 发送消息处理
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="type">接受者类型：0，用户；1，群；2，讨论组</param>
        /// <param name="id">用户：uid；群：qid；讨论组：did</param>
        /// <param name="messageToSend">要发送的消息</param>
        /// <returns></returns>
      // internal static bool Message_Send(int type, string id, string messageToSend, bool auto = true)
        public  static bool Message_Send(int type, string id, string messageToSend, bool auto = true)
        {
            if (auto)
            {
                if (type == 0)
                {
                 //   Program.MainForm.AddAndReNewTextBoxFriendChat(id, ("自动回复：" + Environment.NewLine + messageToSend));
                }
                  else if (type == 1)
                {}
                 //   Program.MainForm.AddAndReNewTextBoxGroupChat(id, ("自动回复：" + Environment.NewLine + messageToSend));
                else if (type == 2)
                { }
                  //  Program.MainForm.AddAndReNewTextBoxDiscussChat(id, ("自动回复：" + Environment.NewLine + messageToSend));
            }
          //  Program.MainForm.listBoxLog.Items.Add(type + ":" + id + ":" + messageToSend);
            if (messageToSend.Equals("") || id.Equals(""))
                return false;

            string[] tmp = messageToSend.Split("{}".ToCharArray());
            messageToSend = "";
            for (int i = 0; i < tmp.Length; i++)
                if (!tmp[i].Trim().StartsWith("..[face") || !tmp[i].Trim().EndsWith("].."))
                    messageToSend += "\\\"" + tmp[i] + "\\\",";
                else
                    messageToSend += tmp[i].Replace("..[face", "[\\\"face\\\",").Replace("]..", "],");
            messageToSend = messageToSend.Remove(messageToSend.LastIndexOf(','));
            messageToSend = messageToSend.Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", "\n").Replace("\n", Environment.NewLine);
            try
            {
                string to_groupuin_did, url;
                switch (type)
                {
                    case 0:
                        to_groupuin_did = "to";
                        url = "http://d1.web2.qq.com/channel/send_buddy_msg2";
                        break;
                    case 1:
                        to_groupuin_did = "group_uin";
                        url = "http://d1.web2.qq.com/channel/send_qun_msg2";
                        break;
                    case 2:
                        to_groupuin_did = "did";
                        url = "http://d1.web2.qq.com/channel/send_discu_msg2";
                        break;
                    default:
                        return false;
                }
                string postData = "{\"#{type}\":#{id},\"content\":\"[#{msg},[\\\"font\\\",{\\\"name\\\":\\\"繁体\\\",\\\"size\\\":10,\\\"style\\\":[0,0,0],\\\"color\\\":\\\"000000\\\"}]]\",\"face\":#{face},\"clientid\":53999199,\"msg_id\":#{msg_id},\"psessionid\":\"#{psessionid}\"}";
                postData = "r=" + HttpUtility.UrlEncode(postData.Replace("#{type}", to_groupuin_did).Replace("#{id}", id).Replace("#{msg}", messageToSend).Replace("#{face}", SelfInfo.face.ToString()).Replace("#{msg_id}", rand.Next(10000000, 99999999).ToString()).Replace("#{psessionid}", psessionid));

                string dat = HTTP.Post(url, postData, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");

                return dat.Equals("{\"errCode\":0,\"msg\":\"send ok\"}");
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// SendClusterMessage
        /// </summary>
        /// <param name="id">QQ</param>
        /// <param name="messageToSend">消息</param>
        /// <param name="type"></param>
        /// <param name="auto"></param>
        /// <returns></returns>
        public static bool SendMessage( string id, string messageToSend, int type=0,bool auto = true)
        {
           id =  RealQQuin[id].ToString ();
            if (messageToSend.Equals("") || id.Equals(""))
                return false;

            string[] tmp = messageToSend.Split("{}".ToCharArray());
            messageToSend = "";
            for (int i = 0; i < tmp.Length; i++)
                if (!tmp[i].Trim().StartsWith("..[face") || !tmp[i].Trim().EndsWith("].."))
                    messageToSend += "\\\"" + tmp[i] + "\\\",";
                else
                    messageToSend += tmp[i].Replace("..[face", "[\\\"face\\\",").Replace("]..", "],");
            messageToSend = messageToSend.Remove(messageToSend.LastIndexOf(','));
            messageToSend = messageToSend.Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", "\n").Replace("\n", Environment.NewLine);
            try
            {
                string to_groupuin_did, url;
                switch (type)
                {
                    case 0:
                        to_groupuin_did = "to";
                        url = "http://d1.web2.qq.com/channel/send_buddy_msg2";
                        break;
                    case 1:
                        to_groupuin_did = "group_uin";
                        url = "http://d1.web2.qq.com/channel/send_qun_msg2";
                        break;
                    case 2:
                        to_groupuin_did = "did";
                        url = "http://d1.web2.qq.com/channel/send_discu_msg2";
                        break;
                    default:
                        return false;
                }
                string postData = "{\"#{type}\":#{id},\"content\":\"[#{msg},[\\\"font\\\",{\\\"name\\\":\\\"繁体\\\",\\\"size\\\":10,\\\"style\\\":[0,0,0],\\\"color\\\":\\\"000000\\\"}]]\",\"face\":#{face},\"clientid\":53999199,\"msg_id\":#{msg_id},\"psessionid\":\"#{psessionid}\"}";
                postData = "r=" + HttpUtility.UrlEncode(postData.Replace("#{type}", to_groupuin_did).Replace("#{id}", id).Replace("#{msg}", messageToSend).Replace("#{face}", SelfInfo.face.ToString()).Replace("#{msg_id}", rand.Next(10000000, 99999999).ToString()).Replace("#{psessionid}", psessionid));

                string dat = HTTP.Post(url, postData, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");

                return dat.Equals("{\"errCode\":0,\"msg\":\"send ok\"}");
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 群消息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="messageToSend"></param>
        /// <param name="type"></param>
        /// <param name="auto"></param>
        /// <returns></returns>
        public static bool SendClusterMessage(string id, string messageToSend, int type = 1, bool auto = true)
        {
            id = RealQQNum[id].ToString();
            if (messageToSend.Equals("") || id.Equals(""))
                return false;

            string[] tmp = messageToSend.Split("{}".ToCharArray());
            messageToSend = "";
            for (int i = 0; i < tmp.Length; i++)
                if (!tmp[i].Trim().StartsWith("..[face") || !tmp[i].Trim().EndsWith("].."))
                    messageToSend += "\\\"" + tmp[i] + "\\\",";
                else
                    messageToSend += tmp[i].Replace("..[face", "[\\\"face\\\",").Replace("]..", "],");
            messageToSend = messageToSend.Remove(messageToSend.LastIndexOf(','));
            messageToSend = messageToSend.Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", "\n").Replace("\n", Environment.NewLine);
            try
            {
                string to_groupuin_did, url;
                switch (type)
                {
                    case 0:
                        to_groupuin_did = "to";
                        url = "http://d1.web2.qq.com/channel/send_buddy_msg2";
                        break;
                    case 1:
                        to_groupuin_did = "group_uin";
                        url = "http://d1.web2.qq.com/channel/send_qun_msg2";
                        break;
                    case 2:
                        to_groupuin_did = "did";
                        url = "http://d1.web2.qq.com/channel/send_discu_msg2";
                        break;
                    default:
                        return false;
                }
                string postData = "{\"#{type}\":#{id},\"content\":\"[#{msg},[\\\"font\\\",{\\\"name\\\":\\\"繁体\\\",\\\"size\\\":10,\\\"style\\\":[0,0,0],\\\"color\\\":\\\"000000\\\"}]]\",\"face\":#{face},\"clientid\":53999199,\"msg_id\":#{msg_id},\"psessionid\":\"#{psessionid}\"}";
                postData = "r=" + HttpUtility.UrlEncode(postData.Replace("#{type}", to_groupuin_did).Replace("#{id}", id).Replace("#{msg}", messageToSend).Replace("#{face}", SelfInfo.face.ToString()).Replace("#{msg_id}", rand.Next(10000000, 99999999).ToString()).Replace("#{psessionid}", psessionid));

                string dat = HTTP.Post(url, postData, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");

                return dat.Equals("{\"errCode\":0,\"msg\":\"send ok\"}");
            }
            catch (Exception)
            {
                return false;
            }
        }
        /// <summary>
        /// 讨论组消息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="messageToSend"></param>
        /// <param name="type"></param>
        /// <param name="auto"></param>
        /// <returns></returns>
        public static bool SendSubjectMessage(string id, string messageToSend, int type = 2, bool auto = true)
        {
            id = RealQQNum[id].ToString();
            if (messageToSend.Equals("") || id.Equals(""))
                return false;

            string[] tmp = messageToSend.Split("{}".ToCharArray());
            messageToSend = "";
            for (int i = 0; i < tmp.Length; i++)
                if (!tmp[i].Trim().StartsWith("..[face") || !tmp[i].Trim().EndsWith("].."))
                    messageToSend += "\\\"" + tmp[i] + "\\\",";
                else
                    messageToSend += tmp[i].Replace("..[face", "[\\\"face\\\",").Replace("]..", "],");
            messageToSend = messageToSend.Remove(messageToSend.LastIndexOf(','));
            messageToSend = messageToSend.Replace("\r\n", "\n").Replace("\n\r", "\n").Replace("\r", "\n").Replace("\n", Environment.NewLine);
            try
            {
                string to_groupuin_did, url;
                switch (type)
                {
                    case 0:
                        to_groupuin_did = "to";
                        url = "http://d1.web2.qq.com/channel/send_buddy_msg2";
                        break;
                    case 1:
                        to_groupuin_did = "group_uin";
                        url = "http://d1.web2.qq.com/channel/send_qun_msg2";
                        break;
                    case 2:
                        to_groupuin_did = "did";
                        url = "http://d1.web2.qq.com/channel/send_discu_msg2";
                        break;
                    default:
                        return false;
                }
                string postData = "{\"#{type}\":#{id},\"content\":\"[#{msg},[\\\"font\\\",{\\\"name\\\":\\\"繁体\\\",\\\"size\\\":10,\\\"style\\\":[0,0,0],\\\"color\\\":\\\"000000\\\"}]]\",\"face\":#{face},\"clientid\":53999199,\"msg_id\":#{msg_id},\"psessionid\":\"#{psessionid}\"}";
                postData = "r=" + HttpUtility.UrlEncode(postData.Replace("#{type}", to_groupuin_did).Replace("#{id}", id).Replace("#{msg}", messageToSend).Replace("#{face}", SelfInfo.face.ToString()).Replace("#{msg_id}", rand.Next(10000000, 99999999).ToString()).Replace("#{psessionid}", psessionid));

                string dat = HTTP.Post(url, postData, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");

                return dat.Equals("{\"errCode\":0,\"msg\":\"send ok\"}");
            }
            catch (Exception)
            {
                return false;
            }
        }
        #endregion

        #region 获取列表保存
        #region 获取好友列表并保存
        /// <summary>
        /// 获取好友列表并保存
        /// </summary>
        // public  static void Info_FriendList()
        internal static void Info_FriendList()
        {
            string url = "http://s.web2.qq.com/api/get_user_friends2";
            string sendData = string.Format("r={{\"vfwebqq\":\"{0}\",\"hash\":\"{1}\"}}", vfwebqq, hash);
            string dat = HTTP.Post(url, sendData, "http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1");

            JsonFriendModel friend = (JsonFriendModel)JsonConvert.DeserializeObject(dat, typeof(JsonFriendModel));
            for (int i = 0; i < friend.result.info.Count; i++)
            {
                if (!FriendList.ContainsKey(friend.result.info[i].uin))
                    FriendList.Add(friend.result.info[i].uin, new FriendInfo());
                FriendList[friend.result.info[i].uin].face = friend.result.info[i].face;
                FriendList[friend.result.info[i].uin].nick = friend.result.info[i].nick;
                Info_FriendInfo(friend.result.info[i].uin);
            }
            for (int i = 0; i < friend.result.friends.Count; i++)
            {
                if (!FriendList.ContainsKey(friend.result.friends[i].uin))
                    FriendList.Add(friend.result.friends[i].uin, new FriendInfo());
                FriendList[friend.result.friends[i].uin].categories = friend.result.friends[i].categories;
            }
            for (int i = 0; i < friend.result.categories.Count; i++)
            {
                FriendCategories[friend.result.categories[i].index] = friend.result.categories[i].name;
            }
           
           // Program.MainForm.ReNewListBoxFriend();
        }


        #endregion
        #region 获取好友的详细信息
        /// <summary>
        /// 获取好友的详细信息
        /// </summary>
        /// <param name="uin"></param>
        public  static void Info_FriendInfo(string uin)
             //  internal static void Info_FriendInfo(string uin)
        {
            string url = "http://s.web2.qq.com/api/get_friend_info2?tuin=#{uin}&vfwebqq=#{vfwebqq}&clientid=53999199&psessionid=#{psessionid}&t=#{t}".Replace("#{t}", AID_TimeStamp());
            url = url.Replace("#{uin}", uin).Replace("#{vfwebqq}", vfwebqq).Replace("#{psessionid}", psessionid);
            string dat = HTTP.Get(url, "http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1");
            JsonFriendInfModel inf = (JsonFriendInfModel)JsonConvert.DeserializeObject(dat, typeof(JsonFriendInfModel));
            if (!FriendList.ContainsKey(uin))
                FriendList.Add(uin, new FriendInfo());
            FriendList[uin].face = inf.result.face;
            FriendList[uin].occupation = inf.result.occupation;
            FriendList[uin].phone = inf.result.phone;
            FriendList[uin].college = inf.result.college;
            FriendList[uin].blood = inf.result.blood;
            FriendList[uin].homepage = inf.result.homepage;
            FriendList[uin].vip_info = inf.result.vip_info;
            FriendList[uin].country = inf.result.country;
            FriendList[uin].city = inf.result.city;
            FriendList[uin].personal = inf.result.personal;
            FriendList[uin].nick = inf.result.nick;
            FriendList[uin].shengxiao = inf.result.shengxiao;
            FriendList[uin].email = inf.result.email;
            FriendList[uin].province = inf.result.province;
            FriendList[uin].gender = inf.result.gender;
            if (inf.result.birthday.year != 0 && inf.result.birthday.month != 0 && inf.result.birthday.day != 0)
                FriendList[uin].birthday = new DateTime(inf.result.birthday.year, inf.result.birthday.month, inf.result.birthday.day);
        }
        #endregion
        #region 获取自己的信息
        #region 获取群成员列表
        /// <summary>
        /// 获取群成员列表
        /// </summary>
        /// <param name="code">群code</param>
        /// <returns>群成员json数据</returns>
        public static  string GetGroupMemberList(string code)
        {
            //http://s.web2.qq.com/api/get_group_info_ext2?gcode=1985076919&vfwebqq=45f5c4e69386c361ec15bf7f23b39c5f9c8198faa3d5417ce484778b5dd678005ae0bb487290d6f5&t=1529999200952
            string ret = HTTP.Get("http://s.web2.qq.com/api/get_group_info_ext2?gcode=" + code + "&vfwebqq=" + vfwebqq + "&t=" + GetRnd());
            return ret;

        }
        #endregion

        /// <summary>
        /// 获取自己的信息
        /// </summary>
        public  static void Info_SelfInfo()
           //  internal static void Info_SelfInfo()
        {
            string url = "http://s.web2.qq.com/api/get_self_info2?t=#{t}".Replace("#{t}", AID_TimeStamp());
            string dat = HTTP.Get(url, "http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1");
            JsonFriendInfModel inf = (JsonFriendInfModel)JsonConvert.DeserializeObject(dat, typeof(JsonFriendInfModel));

            SelfInfo.face = inf.result.face;
            SelfInfo.occupation = inf.result.occupation;
            SelfInfo.phone = inf.result.phone;
            SelfInfo.college = inf.result.college;
            SelfInfo.blood = inf.result.blood;
            SelfInfo.homepage = inf.result.homepage;
            SelfInfo.vip_info = inf.result.vip_info;
            SelfInfo.country = inf.result.country;
            SelfInfo.city = inf.result.city;
            SelfInfo.personal = inf.result.personal;
            SelfInfo.nick = inf.result.nick;
            SelfInfo.shengxiao = inf.result.shengxiao;
            SelfInfo.email = inf.result.email;
            SelfInfo.province = inf.result.province;
            SelfInfo.gender = inf.result.gender;
            if (inf.result.birthday.year != 0 && inf.result.birthday.month != 0 && inf.result.birthday.day != 0)
                SelfInfo.birthday = new DateTime(inf.result.birthday.year, inf.result.birthday.month, inf.result.birthday.day);
        }
        #endregion

        #region 获取群列表并保存
        /// <summary>
        /// 获取群列表并保存
        /// </summary>
        public static void Info_GroupList()
            //  internal static void Info_GroupList()
        {
            string url = "http://s.web2.qq.com/api/get_group_name_list_mask2";
            string sendData = string.Format("r={{\"vfwebqq\":\"{0}\",\"hash\":\"{1}\"}}", vfwebqq, hash);
            string dat = HTTP.Post(url, sendData, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");

            JsonGroupModel group = (JsonGroupModel)JsonConvert.DeserializeObject(dat, typeof(JsonGroupModel));
            //SysOnlog("Post群列表成功");
            for (int i = 0; i < group.result.gnamelist.Count; i++)
            {
                if (!GroupList.ContainsKey(group.result.gnamelist[i].gid))
                    GroupList.Add(group.result.gnamelist[i].gid, new GroupInfo());
                GroupList[group.result.gnamelist[i].gid].name = group.result.gnamelist[i].name;
                GroupList[group.result.gnamelist[i].gid].code = group.result.gnamelist[i].code;
                Info_GroupInfo(group.result.gnamelist[i].gid);
              
               // RuiRui.GetGroupSetting(group.result.gnamelist[i].gid);
            }

           // Program.MainForm.ReNewListBoxGroup();
          //  SysOnlog("获取群列表并保存");
        }
        #endregion

        #region 获取群的详细信息
        /// <summary>
        /// 获取群的详细信息
        /// </summary>
        /// <param name="gid"></param>
        public static void Info_GroupInfo(string gid)
        //  internal static void Info_GroupInfo(string gid)
        {
            if (!GroupList.ContainsKey(gid))
                return;
            string gcode = GroupList[gid].code;
            string url = "http://s.web2.qq.com/api/get_group_info_ext2?gcode=#{group_code}&vfwebqq=#{vfwebqq}&t=#{t}".Replace("#{group_code}", gcode).Replace("#{vfwebqq}", vfwebqq).Replace("#{t}", AID_TimeStamp());
            string dat = HTTP.Get(url, "http://s.web2.qq.com/proxy.html?v=20130916001&callback=1&id=1");
            JsonGroupInfoModel groupInfo = (JsonGroupInfoModel)JsonConvert.DeserializeObject(dat, typeof(JsonGroupInfoModel));
            GroupList[gid].name = groupInfo.result.ginfo.name;
            GroupList[gid].createtime = groupInfo.result.ginfo.createtime;
            GroupList[gid].face = groupInfo.result.ginfo.face;
            GroupList[gid].owner = groupInfo.result.ginfo.owner;
            GroupList[gid].memo = groupInfo.result.ginfo.memo;
            GroupList[gid].markname = groupInfo.result.ginfo.markname;
            GroupList[gid].level = groupInfo.result.ginfo.level;
         //   SysOnlog("Get群的详细信息");
            for (int i = 0; i < groupInfo.result.minfo.Count; i++)
            {
                if (!GroupList[gid].MemberList.ContainsKey(groupInfo.result.minfo[i].uin))
                    GroupList[gid].MemberList.Add(groupInfo.result.minfo[i].uin, new GroupInfo.MenberInfo());
                GroupList[gid].MemberList[groupInfo.result.minfo[i].uin].city = groupInfo.result.minfo[i].city;
                GroupList[gid].MemberList[groupInfo.result.minfo[i].uin].province = groupInfo.result.minfo[i].province;
                GroupList[gid].MemberList[groupInfo.result.minfo[i].uin].country = groupInfo.result.minfo[i].country;
                GroupList[gid].MemberList[groupInfo.result.minfo[i].uin].gender = groupInfo.result.minfo[i].gender;
                GroupList[gid].MemberList[groupInfo.result.minfo[i].uin].nick = groupInfo.result.minfo[i].nick;
            }
            if (groupInfo.result.cards != null)
                for (int i = 0; i < groupInfo.result.cards.Count; i++)
                {
                    if (!GroupList[gid].MemberList.ContainsKey(groupInfo.result.cards[i].muin))
                        GroupList[gid].MemberList.Add(groupInfo.result.cards[i].muin, new GroupInfo.MenberInfo());
                    GroupList[gid].MemberList[groupInfo.result.cards[i].muin].card = groupInfo.result.cards[i].card;
                }
            for (int i = 0; i < groupInfo.result.ginfo.members.Count; i++)
                if (groupInfo.result.ginfo.members[i].mflag % 2 == 1)
                    GroupList[gid].MemberList[groupInfo.result.ginfo.members[i].muin].isManager = true;
                else GroupList[gid].MemberList[groupInfo.result.ginfo.members[i].muin].isManager = false;

          //  SysOnlog("成功获取群详细信息");
        }
        #endregion

        #region
        /// <summary>
        /// 获取自己的群名片
        /// </summary>
        /// <param name="gcode">所在的群gocde</param>
        /// <returns>返回自己在群名片中的信息</returns>
        public static  string GetMyCardInGroup(string gcode)
        {
            return HTTP.Get("http://s.web2.qq.com/api/get_self_business_card2?gcode=" + gcode + "&vfwebqq=" + vfwebqq + "&t=" + GetRnd());
        }
        /// <summary>
        /// 获取QQ等级
        /// </summary>
        /// <param name="uin">好友uin</param>
        /// <returns></returns>
        public static string GetQQlevel(string uin)
        {
            return HTTP.Get("http://s.web2.qq.com/api/get_qq_level2?tuin=" + uin + "&vfwebqq=" + vfwebqq + "&t=" + GetRnd());
        }
        /// <summary>
        /// 获取个人信息
        /// GET
        /// http://s.web2.qq.com/api/get_self_info2?t=1459735133188
        /// </summary>
        /// <returns></returns>
        public static  string self()
        {
            return HTTP.Get("http://s.web2.qq.com/api/get_self_info2?t=" + GetRnd());
        }
#endregion

        #region 自定群详细资料
        /// <summary>
        /// 自定群详细资料
        /// </summary>
        /// <param name="gid"></param>
        internal static void Info_ZDGroupListInfo(string gid)
        {
          //  if (!ZDInfoList.ContainsKey(gid))
         //    return;
           // string gcode = ZDInfoList[gid].code;

        string ret = HTTP.Get("http://s.web2.qq.com/api/get_group_info_ext2?gcode=" + gid + "&vfwebqq=" + vfwebqq + "&t=" + GetRnd());

            JsonGroupInfoZD groupInfo = (JsonGroupInfoZD)JsonConvert.DeserializeObject(ret, typeof(JsonGroupInfoZD));

            for (int i = 0; i < groupInfo.result.minfo.Count; i++)
            {
                ZDInfoList.Add(groupInfo.result.minfo[i].ToString (), new ZDInfo());
                ZDInfoList[gid].city = groupInfo.result.minfo[i].city;
                ZDInfoList[gid].province = groupInfo.result.minfo[i].province;
                ZDInfoList[gid].country = groupInfo.result.minfo[i].country;
                ZDInfoList[gid].gender = groupInfo.result.minfo[i].gender;
                ZDInfoList[gid].nick = groupInfo.result.minfo[i].nick;
            
            }
     
          
        }

        
        #endregion

        #region 获取讨论组列表并保存
        /// <summary>
        /// 获取讨论组列表并保存
        /// </summary>
        internal static void Info_DisscussList()
        {
            string url = "http://s.web2.qq.com/api/get_discus_list?clientid=53999199&psessionid=#{psessionid}&vfwebqq=#{vfwebqq}&t=#{t}".Replace("#{psessionid}", psessionid).Replace("#{vfwebqq}", vfwebqq).Replace("#{t}", AID_TimeStamp());
            string dat = HTTP.Get(url, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");
            JsonDisscussModel disscuss = (JsonDisscussModel)JsonConvert.DeserializeObject(dat, typeof(JsonDisscussModel));
            for (int i = 0; i < disscuss.result.dnamelist.Count; i++)
            {
                if (!DiscussList.ContainsKey(disscuss.result.dnamelist[i].did))
                    DiscussList.Add(disscuss.result.dnamelist[i].did, new DiscussInfo());
                DiscussList[disscuss.result.dnamelist[i].did].name = disscuss.result.dnamelist[i].name;
                Info_DisscussInfo(disscuss.result.dnamelist[i].did);
            }
           // Program.MainForm.ReNewListBoxDiscuss();

        }
        #endregion
        #region 获取讨论组详细信息
        /// <summary>
        /// 获取讨论组详细信息
        /// </summary>
        /// <param name="did"></param>
        internal static void Info_DisscussInfo(string did)
        {
            string url = "http://d1.web2.qq.com/channel/get_discu_info?did=#{discuss_id}&psessionid=#{psessionid}&vfwebqq=#{vfwebqq}&clientid=53999199&t=#{t}".Replace("#{t}", AID_TimeStamp());
            url = url.Replace("#{discuss_id}", did).Replace("#{psessionid}", psessionid).Replace("#{vfwebqq}", vfwebqq);
            string dat = HTTP.Get(url, "http://d1.web2.qq.com/proxy.html?v=20151105001&callback=1&id=2");
            JsonDisscussInfoModel inf = (JsonDisscussInfoModel)JsonConvert.DeserializeObject(dat, typeof(JsonDisscussInfoModel));

            for (int i = 0; i < inf.result.mem_info.Count; i++)
            {
                if (!DiscussList[did].MemberList.ContainsKey(inf.result.mem_info[i].uin))
                    DiscussList[did].MemberList.Add(inf.result.mem_info[i].uin, new DiscussInfo.MenberInfo());
                DiscussList[did].MemberList[inf.result.mem_info[i].uin].nick = inf.result.mem_info[i].nick;
            }
            for (int i = 0; i < inf.result.mem_status.Count; i++)
            {
                if (!DiscussList[did].MemberList.ContainsKey(inf.result.mem_status[i].uin))
                    DiscussList[did].MemberList.Add(inf.result.mem_status[i].uin, new DiscussInfo.MenberInfo());
                DiscussList[did].MemberList[inf.result.mem_status[i].uin].status = inf.result.mem_status[i].status;
                DiscussList[did].MemberList[inf.result.mem_status[i].uin].client_type = inf.result.mem_status[i].client_type;
            }
        }
        #endregion
        #endregion

        #region 跨线程访问控件 在控件上执行委托
        /// <summary>
        /// 跨线程访问控件 在控件上执行委托
        /// </summary>
        /// <param name="ctl">控件</param>
        /// <param name="del">执行的委托</param>
        public static void CrossThreadCalls(this TextBox  ctl, ThreadStart del)
        {
            if (del == null) return;
            if (ctl.InvokeRequired)
                ctl.Invoke(del, null);
            else
                del();
        }
        /// <summary>
        /// 绑定接收消息函数，事件参数是转化后的对象，可以在绑定的函数中使用控件，已经做好了跨线程访问控件处理
        /// </summary>
        /// <param name="form">当前窗体From对象</param>
        /// <param name="ReceiveJsonMsg">触发接受消息的函数,返回json消息</param>
        public static  void BindEventReceivelog(Form form, DelegateReceivelog Receivelog)
        {
            formObject = form;
            EventReceivelog += Receivelog;

        }
        /// <summary>
        /// 绑定接收消息函数，事件参数是转化后的对象，注意不要在绑定的函数中使用控件，请自己处理跨线程访问控件
        /// </summary>
        /// <param name="ReceiveObjectMsg">触发接受消息的函数</param>
        public static  void BindEventReceivelog(DelegateReceivelog Receivelog)
        {
            EventReceivelog += Receivelog;

        }
        #endregion 

        #region 其他测试
        public static void cs()
        {
            string url = "http://qun.qq.com/cgi-bin/group_subscribe/get_group_subscribe_status";
            string sendData = string.Format("r={{\"vfwebqq\":\"{0}\",\"hash\":\"{1}\"}}", vfwebqq, hash);
            string dat = HTTP.Post(url, sendData, "http://web.qun.qq.com/announce/index.html");

            JsonFriendModel friend = (JsonFriendModel)JsonConvert.DeserializeObject(dat, typeof(JsonFriendModel));

        }
        public static void csa()
        {
            string url = "http://qun.qq.com/cgi-bin/qun_mgr/add_group_member";
            string sendData = string.Format("r={{\"vfwebqq\":\"{0}\",\"hash\":\"{1}\"}}", vfwebqq, hash);
            string dat = HTTP.Post(url, sendData, "http://web.qun.qq.com/announce/index.html");

            JsonFriendModel friend = (JsonFriendModel)JsonConvert.DeserializeObject(dat, typeof(JsonFriendModel));

        }
        #endregion

        #region QQ处理
        #region 根据uin获取真实QQ号
        /// <summary>
        /// 根据uin获取真实QQ号
        /// </summary>
        /// <param name="uin"></param>
        /// <returns></returns>
       //  internal static string Info_RealQQ(string uin)
       public  static string Info_RealQQ(string uin)
        {

            if (RealQQNum.ContainsKey(uin))
                return RealQQNum[uin];

            string url = "http://s.web2.qq.com/api/get_friend_uin2?tuin=#{uin}&type=1&vfwebqq=#{vfwebqq}&t=#{t}".Replace("#{uin}", uin).Replace("#{vfwebqq}", vfwebqq).Replace("#{t}", AID_TimeStamp());
            string dat = HTTP.Get(url);

            if (!string .IsNullOrEmpty (dat.Trim ()))
            {

                string temp = dat.Split('\"')[10].Split(',')[0].Replace(":", "");
                if (temp != "" && !RealQQNum.ContainsKey(uin))
                {
                    RealQQNum.Add(uin, temp);
                    RealQQuin.Add(temp, uin);
                    RealQQNum.Add(temp, uin);
                    return temp;

                }
                else return uin; 
            }
            else return uin;
        }
        #endregion


      //  http://s.web2.qq.com/api/get_single_long_nick2?tuin=1260396572&vfwebqq=45f5c4e69386c361ec15bf7f23b39c5f9c8198faa3d5417ce484778b5dd678005ae0bb487290d6f5&t=1529998933455
        #region 根据QQ获取UIN
        /// <summary>
        /// 根据QQ获取UIN
        /// </summary>
        /// <param name="uin"></param>
        /// <returns></returns>
        public  static string Info_Reauin(string qq)
        {
            if (RealQQNum.ContainsKey(qq))
                return RealQQNum[qq];

            string url = "http://s.web2.qq.com/api/get_friend_uin2?tuin=#{uin}&type=1&vfwebqq=#{vfwebqq}&t=#{t}".Replace("#{uin}", uin).Replace("#{vfwebqq}", vfwebqq).Replace("#{t}", AID_TimeStamp());
            string dat = HTTP.Get(url);
            string temp = dat.Split('\"')[10].Split(',')[0].Replace(":", "");

            if (temp != "" && !RealQQuin.ContainsKey(qq))
            {
                RealQQuin.Add(qq, temp);
                return temp;
            }
            else return "";
        }
        #endregion
        #region 根据QQ号和ptwebqq值获取hash值，用于获取好友列表和群列表
        /// <summary>
        /// 根据QQ号和ptwebqq值获取hash值，用于获取好友列表和群列表
        /// </summary>
        /// <param name="QQNum">QQ号</param>
        /// <param name="ptwebqq">ptwebqq</param>
        /// <returns>hash值</returns>
        /// 
        public  static string AID_Hash(string QQNum, string ptwebqq)
        {
            int[] N = new int[4];
            long QQNum_Long = long.Parse(QQNum);
            for (int T = 0; T < ptwebqq.Length; T++)
            {
                N[T % 4] ^= ptwebqq.ToCharArray()[T];
            }
            string[] U = { "EC", "OK" };
            long[] V = new long[4];
            V[0] = QQNum_Long >> 24 & 255 ^ U[0].ToCharArray()[0];
            V[1] = QQNum_Long >> 16 & 255 ^ U[0].ToCharArray()[1];
            V[2] = QQNum_Long >> 8 & 255 ^ U[1].ToCharArray()[0];
            V[3] = QQNum_Long & 255 ^ U[1].ToCharArray()[1];

            long[] U1 = new long[8];

            for (int T = 0; T < 8; T++)
            {
                U1[T] = T % 2 == 0 ? N[T >> 1] : V[T >> 1];
            }

            string[] N1 = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
            string V1 = "";

            for (int i = 0; i < U1.Length; i++)
            {
                V1 += N1[(int)((U1[i] >> 4) & 15)];
                V1 += N1[(int)(U1[i] & 15)];
            }
            return V1;
        }
        #endregion
        #region 获取时间戳
        /// <summary>
        /// 获取时间戳
        /// </summary>
        /// <param name="type">1，10位；2，13位。</param>
        /// <returns></returns>
        public static string AID_TimeStamp(int type = 1)
        {
            if (type == 1)
            {
                DateTime dt1970 = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                return ((DateTime.UtcNow.Ticks - dt1970.Ticks) / 10000).ToString();
            }
            else if (type == 2)
            {
                TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                return Convert.ToInt64(ts.TotalSeconds).ToString();
            }
            else return "ERROR";
        }
        #endregion
        #region 生成由群主QQ和群创建时间构成的群标识码
        /// <summary>
        /// 生成由群主QQ和群创建时间构成的群标识码
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
      //  internal static string AID_GroupKey(string gid)
      public   static string AID_GroupKey(string gid)
        {
            if (!GroupList.ContainsKey(gid))
                Info_GroupList();
            if (GroupList.ContainsKey(gid))
                return Info_RealQQ(GroupList[gid].owner) + ":" + GroupList[gid].createtime;
            else return "FAIL";
        }
        #endregion
        #endregion

        #region 好友资料类
        /// <summary>
        /// 好友资料类
        /// </summary>
        public class FriendInfo
        {
            public string markname;
            public string nick;
            public string gender;

            public int face;
            public int client_type;
            public int categories;
            public string status;

            public string occupation;   //职业                    
            public string college;
            public string country;
            public string province;
            public string city;
            public string personal;     //简介

            public string homepage;
            public string email;
            public string mobile;
            public string phone;

            public DateTime birthday;
            public int blood;
            public int shengxiao;
            public int vip_info;

            public string Messages = "";
        }
        #endregion

        #region zdinfo
        public class ZDInfo
        {
            
                public string nick;
                public string country;
                public string province;
                public string city;
                public string gender;
                public string card;
                public bool isManager;

                public string Messages = "";
            
        }
        #endregion

        #region 群资料类

        /// <summary>
        /// 群资料类
        /// </summary>
        public class GroupInfo
        {
            public string name;
            public string code;
            public string markname;
            public string memo;
            public int face;
            public string createtime;
            public int level;
            public string owner;
            public GroupManageClass GroupManage = new GroupManageClass();
            public class GroupManageClass
            {
                public string enable;
                public string enableWeather;
                public string enableExchangeRate;
                public string enableStock;
                public string enableStudy;
                public string enableTalk;
                public string enableXHJ;
                public string enableEmoje;
                public string enableCityInfo;
                public string enableWiki;
                public string enableTranslate;
            }
            public Dictionary<string, MenberInfo> MemberList = new Dictionary<string, MenberInfo>();
            public class MenberInfo
            {
                public string nick;
                public string country;
                public string province;
                public string city;
                public string gender;
                public string card;
                public bool isManager;
            }
            public string Messages = "";
        }
        #endregion

        # region 讨论组资料类
        /// <summary>
        /// 讨论组资料类
        /// </summary>
        public class DiscussInfo
        {
            public string name;
            public Dictionary<string, MenberInfo> MemberList = new Dictionary<string, MenberInfo>();
            public class MenberInfo
            {
                public string nick;
                public string status;
                public int client_type;
            }
            public string Messages = "";
        }
        #endregion

        #region 停止时间
        public static void Sleep(int a)
        {
             Thread .Sleep(a);
             Application.DoEvents();
        }
        #endregion

        #region 输出日记
        public static  void SendOnlog(string messages, int onlogtf = 0, string qq = "0", string qun = "0")
        {
            // MainForm mainforms = new MainForm();
          
                if (onlogtf == 1)//好友
                {
                    SysOnlog( "\r\n" + DateTime.Now.ToString() + "收到"
                          + "【" + qq + "】的私聊消息：" + messages.Trim());
                }
                else if (onlogtf == 2)//群
                {
                   SysOnlog( "\r\n" + DateTime.Now.ToString() + "收到"
                          + "【" + qq + "】的群消息：" + messages.Trim());
                }
                else if (onlogtf == 3)//讨论组
                {
                   SysOnlog( "\r\n" + DateTime.Now.ToString() + "收到"
                         + "【" + qq + "】的讨论组消息：" + messages.Trim());
                }
                else if (onlogtf == 0)//系统
                {
                    SysOnlog("\r\n" + DateTime.Now.ToString() + "收到【系统消息】" + messages.Trim());
                }
            
        }
   
        public static void SysOnlog(string messages)
        {
            SysLog.LogManager.GetInstance().AddLog(messages);
            //record = messages;
        }
       
        #endregion

        #region 获取随机数
        /// <summary>
        /// 获取随机数
        /// </summary>
        /// <param name="len">随机数长度1-10</param>
        /// <returns></returns>
        public static string GetRnd(int len = 10)
        {
            Random rd = new Random();
            string strrnd = rd.Next().ToString() + rd.Next().ToString();

            if (len > 0 && len <= strrnd.Length)
                return strrnd.Substring(0, len);
            else
                return strrnd.Substring(0, 10);

        }
        #endregion
    }
}
