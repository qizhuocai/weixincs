﻿using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Threading;

namespace QGWebQQSDK
{
    #region 共同静态类
    public static class CFun
    {
 
        private static CEncode ce = new CEncode();

        /// <summary>
        /// 获取随机数
        /// </summary>
        /// <param name="len">随机数长度1-10</param>
        /// <returns></returns>
        public static string GetRnd(int len = 10)
        {
            Random rd = new Random();
            string strrnd = rd.Next().ToString() + rd.Next().ToString();

            if (len > 0 && len <= strrnd.Length)
                return strrnd.Substring(0, len);
            else
                return strrnd.Substring(0, 10);

        }
        /// <summary>
        /// 在线状态
        /// </summary>
        public struct Status
        {
            public const string online = "在线";
            public const string hidden = "隐身";
            public const string busy = "忙碌";
            public const string callme = "Q我吧";
            public const string away = "离线";
            public const string silent = "请勿打扰";
        }
        /// <summary>
        /// 字符串为分隔符分割数组
        /// </summary>
        /// <param name="input"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static string[] Split(string input, string pattern)
        {

            return input.Split(new string[] { pattern }, StringSplitOptions.RemoveEmptyEntries);
        }
        /// <summary>
        /// 阻塞延时
        /// </summary>
        /// <param name="Second">延时多少秒</param>
        public static void Delay(int Second)
        {
            DateTime now = DateTime.Now;
            int s;
            do
            {
                TimeSpan spand = DateTime.Now - now;
                s = spand.Seconds;
            }
            while (s < Second);

        }

        /// <summary>
        /// 从二进制数组保存到文件
        /// </summary>
        /// <param name="ByteArray">二进制数组</param>
        /// <param name="FileName">文件路径</param>
        /// <returns></returns>
        public static bool SaveFileFromByteArray(Byte[] ByteArray, string FileName)
        {
            try
            {
                Stream flstr = new FileStream(FileName, FileMode.Create);
                BinaryWriter sw = new BinaryWriter(flstr, Encoding.Unicode);
                byte[] buffer = { 0, 9, 6 };
                sw.Write(buffer);
                sw.Close(); flstr.Close();
                return true;

            }
            catch
            {
                return false;
            }
        }
        public static long GetTimestamp()
        {
            //double intResult = 0;      
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1, 0, 0, 0, 0));
            //intResult = (time- startTime).TotalMilliseconds;      
            long t = (System.DateTime.Now.Ticks - startTime.Ticks) / 10000;
            //除10000调整为13位          
            return t;
        }
        public static bool SaveFileFromString(string text, string FileName=null)
        {
            try
            {
                FileName = FileName == null ? AppDomain.CurrentDomain.BaseDirectory + "\\test.txt" : FileName;
                //如果文件txt存在就打开，不存在就新建 .append 是追加写
                FileStream fst = new FileStream(FileName, FileMode.Append);
                //写数据到
                StreamWriter swt = new StreamWriter(fst, System.Text.Encoding.GetEncoding("utf-8"));//写入
                swt.WriteLine(text);
                swt.Close();
                fst.Close();
                return true;
            }
            catch {
                return false;
            }
            
        }

        /// <summary>
        /// 执行JS
        /// </summary>
        /// <param name="sExpression">参数体</param>
        /// <param name="sCode">JavaScript代码的字符串</param>
        /// <returns></returns>
        public static string ExecuteScript(string sExpression)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "mq_comm.js";
            string sCode = File.ReadAllText(path);
            MSScriptControl.ScriptControl scriptControl = new MSScriptControl.ScriptControl();
            scriptControl.UseSafeSubset = false;
            scriptControl.Language = "JavaScript";
            scriptControl.AddCode(sCode);
            try
            {
                string str = scriptControl.Eval(sExpression).ToString();
                return str;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return null;
        }


    }
    #endregion

}
