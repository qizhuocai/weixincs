﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace QGWebQQSDK.SysLog
{
    class Log
    {
        /// <summary>
        /// 从二进制数组保存到文件
        /// </summary>
        /// <param name="ByteArray">二进制数组</param>
        /// <param name="FileName">文件路径</param>
        /// <returns></returns>
        public static bool SaveFileFromByteArray(Byte[] ByteArray, string FileName)
        {
            try
            {
                Stream flstr = new FileStream(FileName, FileMode.Create);
                BinaryWriter sw = new BinaryWriter(flstr, Encoding.Unicode);
                byte[] buffer = { 0, 9, 6 };
                sw.Write(buffer);
                sw.Close(); flstr.Close();
                return true;

            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// 默认test文件日记 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public static bool SaveFileFromString(string text, string FileName = null)
        {
            try
            {
                FileName = FileName == null ? AppDomain.CurrentDomain.BaseDirectory + "\\test.txt" : FileName;
                //如果文件txt存在就打开，不存在就新建 .append 是追加写
                FileStream fst = new FileStream(FileName, FileMode.Append);
                //写数据到
                StreamWriter swt = new StreamWriter(fst, System.Text.Encoding.GetEncoding("utf-8"));//写入
                swt.WriteLine(text);
                swt.Close();
                fst.Close();
                return true;
            }
            catch
            {
                return false;
            }

        }
    }
}
