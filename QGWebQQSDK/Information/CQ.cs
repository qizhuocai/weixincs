﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGWebQQSDK.Information
{
    /// <summary>
    /// 提供的基本方法静态类。
    /// </summary>
    public static class CQ
    {
        /// <summary>
        /// 声明对象多线程同步访问锁引用。
        /// </summary>
        [NonSerialized]
        private static Object _syncRoot = null;

        /// <summary>
        ///  C#代理类型。
        /// </summary>
        private static CQProxyType proxyType = CQProxyType.NativeClr;

        /// <summary>
        /// 代理类型。
        /// </summary>
        public static CQProxyType ProxyType
        {
            get
            {
                return CQ.proxyType;
            }
            set
            {
                CQ.proxyType = value;
            }

        }
        /// <summary>
        /// 存储群成员的缓存。
        /// </summary>
      //  private static Dictionary<long, Dictionary<long, CQGroupMemberInfo>> _dicCache = null;

        /// <summary>
        /// 静态构造。
        /// </summary>
        static CQ()
        {
         //   _dicCache = new Dictionary<long, Dictionary<long, CQGroupMemberInfo>>();
          //  _syncRoot = new object();
        }
    }
}
