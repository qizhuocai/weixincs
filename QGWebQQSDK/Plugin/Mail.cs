﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace QGWebQQSDK.Plugin
{
     public  class Mail
    {

         public static  bool SendMail(string sender, string recipient, string body,string subject, string password, string recipienta = null,string attachment=null)
         {
           SmtpClient mailClient = new SmtpClient("smtp.qq.com");
           mailClient.EnableSsl = true;
           //Credentials登陆SMTP服务器的身份验证.
          // mailClient.Credentials = new NetworkCredential("472671180@qq.com", "22");
           mailClient.Credentials = new NetworkCredential(sender, password);
           //test@qq.com发件人地址、test@tom.com收件人地址
             MailMessage message = new MailMessage(new MailAddress(sender), new MailAddress(recipient));
             if (recipienta!=null)
             {
                 message.Bcc.Add(new MailAddress(recipienta)); //可以添加多个收件人
             }

           // message.Bcc.Add(new MailAddress("tst@qq.com")); //可以添加多个收件人
             message.Body = body;//邮件内容
             message.Subject = subject;//邮件主题
           //Attachment 附件
           //   Attachment att = new Attachment(@"H:\码云\0929\dingxiaowei-CSharpSendEmail-master\CSharpSendEmail\ConsoleApplication3\1.txt");
           //	message.Attachments.Add(att);//添加附件
             if (attachment!=null )
             {
              Attachment att = new Attachment(@attachment);
            message.Attachments.Add(att);//添加附件
             }
           //Console.WriteLine("Start Send Mail....");
           //发送....
             try
             { 
                 mailClient.Send(message);
                 return true;
             }
             catch { return false; }
          

           //	Console.WriteLine("Send Mail Successed");

           //Console.ReadLine();

       }

    }

}
