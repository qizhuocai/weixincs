﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGWebQQSDK.Message
{

    /// <summary>
    /// 私聊消息接收事件参数基类。
    /// </summary>
    public class MessageEventArgs : EventArgs
    {
        /// <summary>
        /// 发生的时间。
        /// </summary>
        public string  Time
        {
            get;
            set;
        }

        /// <summary>
        /// 昵称。
        /// </summary>
        public string Nick
        {
            get;
            set;
        }

        /// <summary>
        /// QQ。
        /// </summary>
        public string QQ
        {
            get;
            set;
        }

        /// <summary>
        /// 日志信息。
        /// </summary>
        public string LogMessage
        {
            get;
            set;
        }
        /// <summary>
        /// 私聊信息。
        /// </summary>
        public string Message
        {
            get;
            set;
        }
    }
}
