﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace QGWebQQSDK.Message
{
    /*
    class Picture
    {
        // [mw_shl_code=csharp,true]
        /// <summary>
        /// 模拟表单提交上传图片
        /// </summary>
        /// <returns></returns>
        public string  Upload_Offline_Pic(string fileName)
        {

            string timeStamp = Util.GetTimestamp(DateTime.Now).ToString();
            String time = DateTime.Now.Ticks.ToString("x");
            string url = string.Format("http://weboffline.ftn.qq.com/ftn_access/upload_offline_pic?time={0}", timeStamp);
            string skeys = this.m_httpHelper.CookieString.Substring(this.m_httpHelper.CookieString.IndexOf("skey=") + 5, 10);
            Dictionary<string, string> dic = new Dictionary<string, string>();
            dic.Add("callback", "parent.EQQ.Model.ChatMsg.callbackSendPic");
            dic.Add("locallangid", "2052");
            dic.Add("clientversion", "1409");
            dic.Add("uin", this.Uin.ToString());
            dic.Add("skey", skeys);
            dic.Add("appid", "15000101");
            dic.Add("peeruin", "593023668");
            dic.Add("fileid", this.m_fileId.ToString());
            dic.Add("vfwebqq", this.m_vfwebqq);
            dic.Add("senderviplevel", "0");
            dic.Add("reciverviplevel", "0");
            this.m_fileId++;

            string html = this.m_httpHelper.UploadImage(fileName, url, dic, "file", this.m_httpHelper.CookieContainer);
            Match match = new Regex(@"(?'json'\{.*?\})", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace).Match(html);
            JavaScriptSerializer js = new JavaScriptSerializer();
            Lingchen.EQQ.Entities.PictureStruct off = js.Deserialize<Lingchen.EQQ.Entities.PictureStruct>(match.Groups["json"].Value);

            return off;
        }

        //[/mw_shl_code]      
        //[mw_shl_code=csharp,true]
        /// <summary>
        /// 发送消息,包括好友,群,群成员
        /// </summary>
        /// <param name="messagePacket">封包结构</param>
        /// <returns></returns>
        public bool SendMessage(MessagePacket messagePacket)
        {
            //http://w.qq.com/d/channel/send_buddy_msg2?t=1378986164051
            //http://w.qq.com/d/channel/send_qun_msg2
            string url = string.Format("http://w.qq.com/d/channel/send_{0}_msg2", messagePacket.MessageType == MessageType.Buddy ? "buddy" : "qun"), html = ""; //发送消息
            #region 处理字符串
            JavaScriptSerializer js = new JavaScriptSerializer();
            string text = "";
            ArrayList list = new ArrayList();
            list = TextHandle(messagePacket.Text, messagePacket.FriendPictureList);
            ArrayList font = new ArrayList();
            font.Add("font");
            CustomFontStyle fontstyle = new CustomFontStyle();
            fontstyle.style = new int[] { 0, 0, 0 };
            fontstyle.size = "10";
            string[] fontnames = new string[] { "宋体", "楷体", "微软雅黑", "黑体", "华文仿宋" };
            fontstyle.name = fontnames[new Random().Next(4)];

            fontstyle.color = new Random().Next(100000, 888888).ToString();
            // string fontstylestring=js.Serialize(fontstyle);
            font.Add(fontstyle);
            list.Add(font);
            text = js.Serialize(list);

            #endregion
            string json = "";
            switch (messagePacket.MessageType)
            {
                #region 发送给好友
                case MessageType.Buddy:
                    {
                        Send_Buddy_Msg_R r = new Send_Buddy_Msg_R()
                        {
                            clientid = m_clientId,
                            face = this.FriendList[messagePacket.Receiver[0]].Face,
                            msg_id = this.m_msg_id,
                            psessionid = this.m_psessionid,
                            to = messagePacket.Receiver[0],
                            content = text
                        };
                        json = js.Serialize(r);
                        break;
                    }
                #endregion
                #region 发送到群里
                case MessageType.Cluster:
                    {
                        if (text.Contains("\"cface\""))
                        {
                            string gfaceurl = string.Format("http://d.web2.qq.com/channel/get_gface_sig2?clientid={0}&psessionid={1}&t={2}", this.m_clientId, this.m_psessionid, Util.GetTimestamp(DateTime.Now));
                            html = m_httpHelper.GetHtml(gfaceurl);
                            Lingchen.EQQ.Entities.WebQQ.Get_Gface_Sig2 gface_sig = js.Deserialize<Lingchen.EQQ.Entities.WebQQ.Get_Gface_Sig2>(html);
                            if (gface_sig.retcode == 0)
                            {
                                Send_Qun_Msg_R_Pic r = new Send_Qun_Msg_R_Pic()
                                {
                                    clientid = m_clientId.ToString(),
                                    psessionid = this.m_psessionid,
                                    content = text,
                                    group_code = messagePacket.GCode,
                                    group_uin = messagePacket.Gid,
                                    key = gface_sig.result.gface_key,
                                    sig = gface_sig.result.gface_sig
                                };
                                json = js.Serialize(r);
                            }
                        }
                        else
                        {
                            Send_Qun_Msg_R r = new Send_Qun_Msg_R()
                            {
                                clientid = m_clientId.ToString(),
                                psessionid = this.m_psessionid,
                                content = text,
                                msg_id = this.m_msg_id,
                                group_uin = messagePacket.Gid,
                            };
                            json = js.Serialize(r);
                        }
                        break;
                    }
                #endregion
                #region 发给群成员
                case MessageType.ClusterMember:
                    url = string.Format("http://d.web2.qq.com/channel/get_c2cmsg_sig2?id={0}&to_uin={1}&service_type=0&clientid={2}&psessionid={3}&t={4}"
                    , messagePacket.GCode, messagePacket.Receiver, this.m_clientId, this.m_psessionid, Util.GetTimestamp(DateTime.Now));


                    html = this.m_httpHelper.GetHtml(url);
                    url = "http://d.web2.qq.com/channel/send_sess_msg2";
                    string group_sig = new Regex(@"value"":""(?'value'[^""]*)""").Match(html).Groups["value"].Value;

                    Send_Sess_Msg_R send_Sess_Msg_R = new Send_Sess_Msg_R()
                    {
                        clientid = m_clientId.ToString(),
                        group_sig = group_sig,
                        service_type = 0,
                        face = 527,
                        msg_id = this.m_msg_id,
                        psessionid = this.m_psessionid,
                        to = messagePacket.Receiver[0],
                        content = text
                    };
                    json = js.Serialize(send_Sess_Msg_R);

                    break;
                #endregion
            }

          //  string postString = string.Format("r={0}&psessionid={1}&clientid={2}", HttpUtility.UrlEncode(json), SmartQQ. psessionid, clientId);
          //  html = this.m_httpHelper.GetHtml(url, postString);
          //  CommonResult sendMsgResult = js.Deserialize<CommonResult>(html);
          //  this.m_msg_id++;
            /*   if (sendMsgResult.retcode == 0)
               {
                   if (messagePacket.MessageType == MessageType.Buddy)
                   {
                       if (Send_friend_count >= this.FriendList.Count)
                           Send_friend_count = 1;
                       this.FriendList[messagePacket.Receiver[0]].SendedCount++;
                       Send_friend_count++;
                   }
                   if (messagePacket.MessageType == MessageType.Cluster)
                       if (Send_cluster_count > this.FriendList.Count)
                       {
                           this.Send_cluster_count = 1;
                       }
                   Send_cluster_count++;
                   if (messagePacket.MessageType == MessageType.ClusterMember) Send_member_count++;
                   return true;
               }
               else
               {
                   return false;
               }
        }//[/mw_shl_code]
    }*/
}
