﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QGWebQQSDK.Message
{
    public class MessageManager
    {
        public event EventHandler<MessageEventArgs> MessageWrite = null;
      //  public event EventHandler<CQLogEventArgs> NewLogWritea = null;

        private List<string> logMessages = null;
    

        private static MessageManager _instance = null;

        private MessageManager()
        {
            this.logMessages = new List<string>();
        }

        public static MessageManager GetInstance()
        {
            if (_instance == null)
            {
                _instance = new MessageManager();
            }

            return _instance;
        }

        public List<string> Logs
        {
            get
            {
                return logMessages;
            }
        }


        public   void Addmessage(string qq, string nick, string message, string time)
        {
           // logMessages.Add(message);

          /*  if (logMessages.Count > 100)
            {
                this.logMessages.RemoveAt(0);
            }*/
             //  MessageWrite.GetInvocationList();
             // if (MessageWrite.Equals()) { return; }
             if (this.MessageWrite != null)
            {
                this.MessageWrite(this, new MessageEventArgs() { Message = message, Nick = nick, QQ = qq, Time = time });
            }
        }
       
    }
}
