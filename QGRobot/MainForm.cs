﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Web.Script.Serialization;
using QGWebQQSDK;
using QGWebQQSDK.Plugin;
using QGWebQQSDK.SysLog;
using QGWebQQSDK.Information;
using System.Windows.Media;
using System.Windows;
using System.Threading;
using System.IO;
namespace QGRobot
{

    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();

          

            LogManager.GetInstance().NewLogWrite += CQLogger_NewLogWrite;
            #region 
            try
            {
               // SendOnlog(String.Format("[＃][系统] CSharp代理启动成功，请手动给挂机QQ发送条信息激活酷Q端代理功能。"));

                if (CQAppContainer.GetInstance().Apps.Count > 0)
                {
                    SendOnlog (String.Format(" [＃][系统] 成功加载{0}个应用。",CQAppContainer.GetInstance().Apps.Count));
                }
                else
                {
                    SendOnlog(String.Format(" [％][异常] 没有加载到应用，你可以使用测试功能测试发送消息。"));
                }
            }
            catch
            {
                //  LogManager.GetInstance().AddLog(String.Format("[{0}] [％][异常] CSharp代理启动失败，18139端口被占用，请检查。", DateTime.Now));
            }
            #endregion

        }
        /// <summary>
        /// 选择APP。
        /// </summary>
        private CQAppAbstract _selectApp = null;
        internal static string DicServer = "https://qizhuocai.tunnel.qydev.com/";
        //多个函数要用到的变量
        string pin = string.Empty;
        public static string qunuin;
        public static int onlogtfa = 1;

     /*   private void NewLogWrite(object sender, MessageEventArgs e)
        {
            if (e.Message == "1")
            {
                SendOnlog("MessageEventArgs测试成功");
            }
           // this.AddLog(e.Message );
        }*/
     
        #region  CQ日志
        /// <summary>
        /// 处理CQ日志事件方法。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CQLogger_NewLogWrite(object sender, CQLogEventArgs e)
        {
           this.SendOnlog(e.LogMessage);
            
        }

        /// <summary>
        /// 多线程同步日志。
        /// </summary>
        /// <param name="logMessage"></param>
        public void AddLog(string logMessage)
        {
            if (onlogtfa == 1)
            {
                try
                {

                    this.lsLogs.Items.Add(logMessage);
                    Thread.Sleep(100);
                    this.tblogon.Text += logMessage.ToString();
                    /*   if (this.lsLogs.Items.Count > 100)
                       {
                           this.lsLogs.Items.RemoveAt(0);
                       }*/
                }
                catch { }
            }
            else if (onlogtfa == 2)
            {
            }


            // Dispatcher.Invoke(new Action<string>(PrintLogs), logMessage);
        }
        #region 打印日志
        /// <summary>
        /// 打印日志。
        /// </summary>
        /// <param name="logMessage"></param>
        private void PrintLogs(string logMessage)
        {
            TextBox tbLog = new TextBox();
            tbLog.Text = logMessage;

            SolidColorBrush logColor = Brushes.Black;
            if (logMessage.Contains("[＃]"))
            {
                logColor = Brushes.DeepPink;
            }
            if (logMessage.Contains("[↓]"))
            {
                logColor = Brushes.Green;
            }
            if (logMessage.Contains("[↑]"))
            {
                logColor = Brushes.Blue;
            }
            if (logMessage.Contains("[％]"))
            {
                logColor = Brushes.Red;
            }
            //  tbLog.Foreground = logColor;

            // tblogon .i
            this.lsLogs.Items.Add(tbLog);
            if (this.lsLogs.Items.Count > 100)
            {
                this.lsLogs.Items.RemoveAt(0);
            }

            //滚动到当前日志
            // lsLogs.(tbLog);
            //  this.lsLogs.ScrollIntoView(tbLog);
        }
        #endregion
        #endregion

        #region 更新界面列表
        #region 更新界面列表
        /// <summary>
        /// 更新主界面的QQ群列表
        /// </summary>
        internal void ReNewListBoxGroup()
        {
            SendOnlog("更新主界面的QQ群列表入口");
            //SysOnlog("更新主界面的QQ群列表入口"); SmartQQ.Info_RealQQ(GroupList.Key)
            listBoxGroup.Items.Clear();
            lvgroup.Items.Clear();
            foreach (KeyValuePair<string, SmartQQ.GroupInfo> GroupList in SmartQQ.GroupList)
            {
                listBoxGroup.Items.Add(GroupList.Key
                    + "::" + GroupList.Value.name
                   + "::" + GroupList.Value.createtime
                   + "::" +GroupList.Key
                   + "::" + GroupList.Value.MemberList.Count.ToString()
                      );

                lvgroup.Items.Add(new ListViewItem(new string[] {
                    GroupList.Key.ToString(),
                     GroupList.Key, 
                    GroupList.Value.name.ToString(),
                   GroupList.Value.MemberList.Count.ToString() ,
                     SmartQQ.Info_RealQQ (GroupList.Value .owner.ToString ())
              
              }));

            }
            //  ReNewtbGroup();

            // SendOnlog  ("更新主界面的QQ群列表");

        }
        #endregion



        #region 更新主界面的QQ好友列表
        /// <summary>
        /// 更新主界面的QQ好友列表
        /// </summary>
        internal void ReNewListBoxFriend()
        {
            //   listBoxFriend.Items.Clear();
            lvfriend.Items.Clear();
            foreach (KeyValuePair<string, SmartQQ.FriendInfo> FriendList in SmartQQ.FriendList)
            {
                //istBoxFriend.Items.Add(FriendList.Key + ":" + SmartQQ.Info_RealQQ(FriendList.Key) + ":" + FriendList.Value.nick);
                /*   listBoxFriend.Items.Add(FriendList.Key + ":" + SmartQQ.Info_RealQQ(FriendList.Key) + ":" + FriendList.Value.nick 
                SmartQQ.Info_RealQQ(FriendList.Key).ToString(),
                       );*/

                lvfriend.Items.Add(new ListViewItem(new string[] { 
                 FriendList.Key.ToString(),
               FriendList.Key,
                 FriendList.Value.nick.ToString(),
                 FriendList.Value.city.ToString() 
             
             }));
            }


        }
        #endregion

        #region 更新主界面的讨论组列表
        /// <summary>
        /// 更新主界面的讨论组列表
        /// </summary>
        internal void ReNewListBoxDiscuss()
        {
            listBoxDiscuss.Items.Clear();
            foreach (KeyValuePair<string, SmartQQ.DiscussInfo> DiscussList in SmartQQ.DiscussList)
            {
                listBoxDiscuss.Items.Add(DiscussList.Key + ":" + DiscussList.Value.name);
            }
        }
        #endregion

        #region 登录显示
        private void FormLogin_Load(object sender, EventArgs e)
        {
            // ReNewtbGroup();
            /*   bool NoFile = false;
               byte[] byData = new byte[100000];
               char[] charData = new char[100000];*/
            //在写判断掉线提示并重新允许登录时，遇到了问题：定时器中断里，
            //在跨线程更新UI时，提示异常，查询发现，这是多线程的一个保护措施。由于嫌麻烦，直接关闭。
            Control.CheckForIllegalCrossThreadCalls = false;
            System.Net.ServicePointManager.DefaultConnectionLimit = 500;
            ReNewListBoxDiscuss();
            Thread.Sleep(100);
            ReNewListBoxFriend();
            Thread.Sleep(100);
            ReNewListBoxGroup();
            toolStripStatusLabel1.Text = "当前登录：" + SmartQQ.QQNum;
            sendera.Text = SmartQQ.QQNum;
            lsApps . Columns[6].Width = 0;
            Thread.Sleep(100);
            chajian();//刷新插件
            /* buttonFriendSend.Enabled = true;
             buttonGroupSend.Enabled = true;
             buttonDiscussSend.Enabled = true;*/
        }
        #endregion

        #endregion

        #region 发送消息

        private void buttonFriendSend_Click(object sender, EventArgs e)
        {
            if (listBoxFriend.SelectedItem != null)
            {
                string[] tmp = listBoxFriend.SelectedItem.ToString().Split(':');
                string MessageToSend = textBoxFriendSend.Text;
                textBoxFriendSend.Clear();

                SmartQQ.Message_Send(0, tmp[0], MessageToSend, false);
                SendOnlog("发送私聊消息给： " + SmartQQ.Info_RealQQ(tmp[0]) + ":" + MessageToSend);
                AddAndReNewTextBoxFriendChat(tmp[0], (SmartQQ.QQNum + "手动发送：" + Environment.NewLine + MessageToSend));
                // SendOnlog(MessageToSend,1,);

            }
        }

        private void buttonGroupSend_Click(object sender, EventArgs e)
        {
            if (listBoxGroup.SelectedItem != null)
            {
                string[] tmp = listBoxGroup.SelectedItem.ToString().Split(':');
                string MessageToSend = textBoxGroupSend.Text;
                textBoxGroupSend.Clear();
                //收到【请叫我好人(2237987378)】/【机器人交流(1173454)】的群消息：fd

                SmartQQ.Message_Send(1, tmp[0], MessageToSend, false);

                SendOnlog("发送群消息到： " + SmartQQ.Info_RealQQ(tmp[0]) + ":" + MessageToSend);
                AddAndReNewTextBoxGroupChat(tmp[0], ("手动发送：" + Environment.NewLine + MessageToSend));
            }
        }

        private void buttonDiscussSend_Click(object sender, EventArgs e)
        {
            if (listBoxDiscuss.SelectedItem != null)
            {
                string[] tmp = listBoxDiscuss.SelectedItem.ToString().Split(':');
                string MessageToSend = textBoxDiscussSend.Text;
                textBoxDiscussSend.Clear();

                SmartQQ.Message_Send(2, tmp[0], MessageToSend, false);
                SendOnlog("发送讨论组消息：" + SmartQQ.Info_RealQQ(tmp[0]) + ":" + MessageToSend);
                //  SendOnlog(MessageToSend, 3, SmartQQ.Info_RealQQ(tmp[0]));
                // SysOnlog("发送讨论组消息：" + SmartQQ.Info_RealQQ(tmp[0])+":"+ MessageToSend);
                AddAndReNewTextBoxDiscussChat(tmp[0], ("手动发送：" + Environment.NewLine + MessageToSend));
            }
        }
        #endregion

        #region 消息显示到界面
        /// <summary>
        /// 私聊消息显示
        /// </summary>
        /// <param name="uin"></param>
        /// <param name="str"></param>
        /// <param name="ChangeCurrentUin"></param>
        internal void AddAndReNewTextBoxFriendChat(string uin, string str = "", bool ChangeCurrentUin = false)
        {
            SmartQQ.FriendList[uin].Messages += str + Environment.NewLine;
            if (ChangeCurrentUin || (listBoxFriend.SelectedItem != null && uin.Equals(listBoxFriend.SelectedItem.ToString().Split(':')[0])))
                textBoxFriendChat.Text = SmartQQ.FriendList[uin].Messages;
            // SysOnlog("收到私聊消息：" +  SmartQQ.FriendList[uin].Messages);

        }
        /// <summary>
        /// 群消息 显示
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="str"></param>
        /// <param name="ChangeCurrentGid"></param>
        internal void AddAndReNewTextBoxGroupChat(string gid, string str = "", bool ChangeCurrentGid = false)
        {
            SmartQQ.GroupList[gid].Messages += str + Environment.NewLine;
            if (ChangeCurrentGid || (listBoxGroup.SelectedItem != null && gid.Equals(listBoxGroup.SelectedItem.ToString().Split(':')[0])))
                textBoxGroupChat.Text = SmartQQ.GroupList[gid].Messages;
            //  SysOnlog("收到群消息：" + SmartQQ.GroupList[gid].Messages);

        }

        internal void AddAndReNewTextBoxDiscussChat(string did, string str = "", bool ChangeCurrentDid = false)
        {
            SmartQQ.DiscussList[did].Messages += str + Environment.NewLine;
            if (ChangeCurrentDid || (listBoxDiscuss.SelectedItem != null && did.Equals(listBoxDiscuss.SelectedItem.ToString().Split(':')[0])))
                textBoxDiscussChat.Text = SmartQQ.DiscussList[did].Messages;
            // SysOnlog("收到讨论组消息：" + SmartQQ.DiscussList[did].Messages );
            // SendOnlog(SmartQQ.DiscussList[did].Messages, 3, "【" + SmartQQ.DiscussList[did].name + "(" + SmartQQ.Info_RealQQ(SmartQQ.DiscussList[did].MemberList.Keys.ToString()) + ")】,"讨论组消息");

        }
        #endregion


        #region 选项卡
        /*
        private void listBoxLog_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxLog.Text = listBoxLog.Items[listBoxLog.SelectedIndex].ToString();
        }
        private void listBoxLog_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxLog.SelectedIndex != -1)
                Clipboard.SetDataObject(listBoxLog.Items[listBoxLog.SelectedIndex].ToString());
        }*/
        private void listBoxFriend_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxFriend.SelectedIndex != -1)
                System.Windows.Forms.Clipboard.SetDataObject(listBoxFriend.Items[listBoxFriend.SelectedIndex].ToString());
        }


        private void listBoxGroup_DoubleClick(object sender, EventArgs e)
        {
            if (listBoxGroup.SelectedIndex != -1)
                System.Windows.Forms.Clipboard.SetDataObject(listBoxGroup.Items[listBoxGroup.SelectedIndex].ToString());
        }
        #endregion

        #region  好友 群 讨论组  添加到组件

        private void listBoxFriend_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] tmp = listBoxFriend.SelectedItem.ToString().Split(':');
                AddAndReNewTextBoxFriendChat(tmp[0], "", true);
            }
            catch { }
        }

        private void listBoxGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] tmp = listBoxGroup.SelectedItem.ToString().Split(':');
                AddAndReNewTextBoxGroupChat(tmp[0], "", true);
            }
            catch { }
        }

        private void listBoxDiscuss_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string[] tmp = listBoxDiscuss.SelectedItem.ToString().Split(':');
                AddAndReNewTextBoxDiscussChat(tmp[0], "", true);
            }
            catch { }
        }
        #endregion

        #region  窗口事件处理

        #region 私有方法　处理窗体的　显示　隐藏　关闭(退出)
        private void ExitMainForm()
        {
            if (MessageBox.Show("是否退出WebQQ？", "确认退出", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.OK)
            {
                this.notifyIcon1.Visible = false;
                this.Close();
                this.Dispose();
                Application.Exit();
            }
        }

        private void HideMainForm()
        {
            this.Hide();
        }

        private void ShowMainForm()
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.Activate();
        }
        #endregion

        #region 右键菜单处理，显示　隐藏　退出
        private void menu_Notify_Click(object sender, EventArgs e)
        {
            ShowMainForm();
        }
        private void 隐藏ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HideMainForm();
        }

        private void 退出ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.notifyIcon1.Visible = false;
            ExitMainForm();
        }
        #endregion

        #region 双击托盘上图标时，显示窗体
        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Minimized;

                HideMainForm();
            }
            else if (this.WindowState == FormWindowState.Minimized)
            {
                ShowMainForm();
            }
        }
        #endregion

        #region 点最小化按钮时，最小化到托盘
        private void MainForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
            {
                HideMainForm();
            }
        }
        #endregion

        #region 窗体关闭时最小化到托盘
        private void MainForm_FormClosing_1(object sender, FormClosingEventArgs e)
        {

            e.Cancel = true;

            HideMainForm();
        }
        #endregion

        #region 双击显示
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowMainForm();
        }
        #endregion

        #endregion

        #region 日志处理


        public void SendOnlog(string messages, int onlogtf = 0, string qq = "0", string qun = "0")
        {
            // MainForm mainforms = new MainForm();
            if (onlogtfa == 1)
            {
                if (onlogtf == 1)//好友
                {
                    AddLog("\r\n" + DateTime.Now.ToString() + "收到"
                          + "【" + qq + "】的私聊消息：" + messages.Trim());
                }
                else if (onlogtf == 2)//群
                {
                    AddLog("\r\n" + DateTime.Now.ToString() + "收到"
                           + "【" + qq + "】的群消息：" + messages.Trim());
                }
                else if (onlogtf == 3)//讨论组
                {
                    AddLog("\r\n" + DateTime.Now.ToString() + "收到"
                          + "【" + qq + "】的讨论组消息：" + messages.Trim());
                }
                else if (onlogtf == 0)//系统
                {
                    AddLog("\r\n" + DateTime.Now.ToString() + "收到【系统消息】" + messages.Trim());
                }
            }
            else if (onlogtfa == 2)
            {

            }
        }
        private void 清空ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            tblogon.Clear();
            SendOnlog("清空日志");
            //  Program.MainForm.tblogon.Text += "\r\n" + "清空日志";

        }


        private void 停止ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            // Program.MainForm.lboplog.Items.Insert(0, "停止日志");
            SendOnlog("停止日志");
            onlogtfa = 2;
            // tblogon.Text += "\r\n" + "停止日志";

        }

        private void 开始日志ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SendOnlog("开始日志");
            onlogtfa = 1;
            //   Program.MainForm.lboplog.Items.Insert(0, "开始日志");
            // tblogon.Text += "\r\n" + "开始日志";

        }
        #endregion

        #region 将json数据反序列化为Dictionary
        /// <summary>

        /// 将json数据反序列化为Dictionary

        /// </summary>

        /// <param name="jsonData">json数据</param>

        /// <returns></returns>

        private Dictionary<string, object> JsonToDictionary(string jsonData)
        {

            //实例化JavaScriptSerializer类的新实例

            JavaScriptSerializer jss = new JavaScriptSerializer();

            try
            {

                //将指定的 JSON 字符串转换为 Dictionary<string, object> 类型的对象

                return jss.Deserialize<Dictionary<string, object>>(jsonData);

            }

            catch (Exception ex)
            {

                throw new Exception(ex.Message);

            }

        }

        #endregion

        #region 测试授权信息
        public void authorization()
        {
            string url = "https://qizhuocai.tunnel.qydev.com/authorization.php?qq=" + SmartQQ.QQNum + "&adminqq=" + SmartQQ.QQNum + "&newtime=" + DateTime.Now.ToShortDateString().ToString();
            /*
                     WebRequest request = WebRequest.Create(url);//实例化WebRequest对象
                     WebResponse response = request.GetResponse();//创建WebResponse对象
                     Stream datastream = response.GetResponseStream();//创建流对象
                     Encoding ec = Encoding.UTF32;
                     Encoding EC = Encoding.Default;
                     StreamReader reader = new StreamReader(datastream, EC);
                     string sqq = reader.ReadToEnd();//读取数据
                     //  richTextBox1.AppendText(responseFromServer);//添加到RichTextBox控件中
                     reader.Close();
                     datastream.Close();*/

            //  string sqq = HTTP.Get("https://qizhuocai.tunnel.qydev.com/authorization.php?qq=" + SmartQQ.QQNum + "&adminqq=" + SmartQQ.QQNum + "&newtime=" + DateTime.Now.ToShortDateString().ToString());
            string sqq = HTTP.Get(url);
            if (sqq == "not")
            {
                return;
            }
            else
            {
                toolStripStatusLabel2.Text = "授权到期时间：" + sqq;
                toolStripStatusLabel1.Text = "现在时间：" + DateTime.Now.ToString();

            }
            //  HTTP.Get("https://qizhuocai.tunnel.qydev.com/infreport.php?qq=" + SmartQQ.QQNum + "&adminqq=" + 111);
        }
        #endregion

        #region 群右键
        private void 管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            #region

            //   lvgroup . BeginUpdate();
            //   lvFaq.BeginUpdate();
            if (lvgroup.Created == true)
            {

                List<string> c = new List<string>();
                for (int i = 0; i < lvgroup.SelectedItems.Count; i++)
                {
                    var item = lvgroup.SelectedItems[i];

                    //  c .Add(item.SubItems[0].Text);
                    qunuin = item.SubItems[0].Text.Trim();

                }
                //  SmartQQ.Info_ZDGroupListInfo(qunuin);
                qunziliao aa = new qunziliao();
                aa.ShowDialog();

                SendOnlog("打开群员界面中。。。");
            }
            #endregion

        }
        private void 刷新群列表ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //  SmartQQ. Info_GroupList();
            listBoxGroup.Items.Clear();
            lvgroup.Items.Clear();
            ReNewListBoxGroup();
        }
        #endregion

        #region 好友右键
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            lvfriend.Items.Clear();
            ReNewListBoxFriend();
            //SmartQQ.  Info_FriendList();
        }

        #endregion
        public void button1_Click(object sender, EventArgs e)
        {

            string send = sendera.Text.ToString().Trim();
            string pass = password.Text.ToString().Trim();
            string bod = body.Text.ToString().Trim();
            string sub = subject.Text.ToString().Trim();
            string recip = recipient.Text.ToString().Trim();
            string recipa = recipienta.Text.ToString().Trim();
            string fu = fujian.Text.ToString().Trim();
            if (!send.EndsWith("@qq.com"))
            {
                send += "@qq.com";
            }
            if (!recip.EndsWith("@qq.com"))
            {
                recip += "@qq.com";
            }
            if (!recipa.EndsWith("@qq.com"))
            {
                recipa += "@qq.com";
            }
            if (send == "" || pass == "" || bod == "" || sub == "" || recip == "")
            {
                MessageBox.Show("不能空");
                return;
            }
            else if (Mail.SendMail(send, recip, bod, sub, pass))
            {
                SendOnlog(send + "对" + recip + "发送邮件成功");
            }
            else
            {
                SendOnlog(send + "对" + recip + "发送邮件失败");
            }
            //string  a = "270954350";
            //  textBox1.Text = SmartQQ.Info_RealQQ(a);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //    string a = "270954350";textBox1.Text = SmartQQ.Info_Reauin(a);
        }


        private void button3_Click(object sender, EventArgs e)
        {
         body .Text=  SmartQQ.GetGroupMemberList(fujian.Text.Trim());
            //SmartQQ.csa();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = "现在时间：" + DateTime.Now.ToString();

        }
        /// <summary>
        /// 启用插件。
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 安装ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // if (this._selectApp != null)
            // {}
            try
            {
               // this._selectApp.RunningStatus = !this._selectApp.RunningStatus;
                if (this._selectApp.RunningStatus)
                {
                    this._selectApp.RunningStatus = false ;
                }
                else 
                { this._selectApp.RunningStatus = true ; }

                string pluginFlieName = Path.GetFileNameWithoutExtension(this._selectApp.AssemblyPath);

                CSPluginsConfigManager.GetInstance().SetLoadingStatus(pluginFlieName, this._selectApp.RunningStatus);
                chajian();
               // 刷新ToolStripMenuItem_Click(刷新ToolStripMenuItem, new RoutedEventArgs());
            }
            catch
            {

            }
        }

        private void 设置ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //  Form testWindow = new Form();
            // testWindow.ShowDialog();
            if (this._selectApp != null)
            {
                this._selectApp.OpenSettingForm();
            }

        }
        #region 刷新插件 可用
        public   void chajian()
        {
            //   this._selectApp = null;
            this.lsApps.Items.Clear();
            CQAppContainer.GetInstance().ReloadApps();
      
            try
            {
                foreach (var item in CQAppContainer.GetInstance().Apps)
                {
                   
                    ListViewItem lvi = new ListViewItem(item.Name);

                    lvi.Tag = item;
                    // lvi.SubItems.Add(i.ToString()); //编号
                    //lvi.SubItems.Add(item.Name);//插件名
                    lvi.SubItems.Add(item.RunningStatus.ToString());//状态
                    lvi.SubItems.Add(item.Description.ToString());//描述 介绍
                    lvi.SubItems.Add(item.Version.ToString()); //版本
                    lvi.SubItems.Add(item.Author);//作者
                    lvi.SubItems.Add("是");//.net
                    lvi.SubItems.Add(item.AssemblyPath.ToString());//程序路径
                    //  this.dataList.Items.Add(lvi);
                    lsApps.Items.Add(lvi);
                }
            }
            catch { }
            //   this.lsApps.ItemsSource = CQAppContainer.GetInstance().Apps;
        }
        private void 刷新ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            chajian();
        }
        #endregion
        #region 删除DLL
        /// <summary>
        /// 应用卸载按钮事件处理方法
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void 删除ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this._selectApp != null)
            {
                if (MessageBox.Show("卸载插件将删除插件对应的Dll文件，是否继续？", "询问", MessageBoxButtons.OKCancel) == DialogResult.OK)
                {
                    File.Delete(this._selectApp.AssemblyPath);
                    this.刷新ToolStripMenuItem_Click(this.刷新ToolStripMenuItem, null);
                }
                // MessageBoxResult result = MessageBox.Show("卸载插件将删除插件对应的Dll文件，是否继续？", "询问", MessageBoxButton.YesNo);


            }
            else
            {
                MessageBox.Show("请先选择要卸载的应用。", "提示");
            }

        }
        #endregion
        #region  目录
        private void 目录ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string pluginFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "CSharpPlugins");

            //目录不存在则返回空列表。
            if (!Directory.Exists(pluginFolder))
            {
                Directory.CreateDirectory(pluginFolder);
            }
            System.Diagnostics.Process.Start("explorer.exe", pluginFolder);
        }
        #endregion

        private void lsApps_SelectedIndexChanged(object sender, EventArgs e)
        {
            ListViewItem LVItems = lsApps.FocusedItem;
             string str= LVItems.SubItems[6].Text;
             if (str.Trim() == lsApps.FocusedItem.SubItems[6].Text.Trim())
             {
                 if (lsApps.FocusedItem.SubItems[1].Text.Trim() == "True")
                 {
                     安装ToolStripMenuItem.Text = "卸载";
                 }
                 else
                 {
                     安装ToolStripMenuItem.Text = "安装";;
                 }
             }
             if (str.Trim () == lsApps.FocusedItem.SubItems[6].Text.Trim ())
            {
                foreach (var  item in CQAppContainer.GetInstance().Apps)
                {
                    if (item.AssemblyPath.ToString() == str)
                    { this._selectApp = item as CQAppAbstract; break; }
                }
            }
          
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {

        }










    }

}
