﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QGWebQQSDK;


namespace QGRobot
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
       
        public CWebQQ cw = new CWebQQ();
        public CEncode ce = new CEncode();
        public CHttpWeb ch = new CHttpWeb();
    

     
        private void RefreshQRCodeImage()
        {
          //  pictureBox1.Image = cw.GetLoginQRCodeImage();
            pictureBox1.Image = SmartQQ.Login_GetQRCode();
         // cw.StartCheckQRCodePoll();
           SmartQQ.Login();
        }
        public void ReceiveLoginStatus(SmartQQ.LoginStatus status)
        {
            switch (status)
            {
                case SmartQQ.LoginStatus.Invalid:
                    label1.Text = "当前登录状态：二维码失效，请稍后";
                    //  RefreshQRCodeImage();
                    break;
                case SmartQQ.LoginStatus.Checking:
                    label1.Text = "当前登录状态：二维码有效，请扫描";
                    break;
                case SmartQQ.LoginStatus.Valid:
                    label1.Text = "当前登录状态：二维码已扫描，请确认";
                    break;
                case SmartQQ.LoginStatus.LoginSuccess:
                    label1.Text = "当前登录状态：确认成功，请稍候";
                    this.Hide();
                    MainForm fMan = new MainForm();
                    fMan.Show();
                    break;
              
            }

        }
        private void Form1_Load(object sender, EventArgs e)
        {
          //  Debug.WriteLine(CQQHelper.getNewHash("958796636", "1f18fc6a9d6534ae1ce4369c51fd6bd6124eab97bcd2b84c0073fea679d30b6d"));
          //  this.Show();

            SmartQQ.BindEventLoginStatus(this, ReceiveLoginStatus);
           // cw.SetHttpWeb(ch);//设置WebQQ对象当前使用的网络对象
            RefreshQRCodeImage();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            //SmartQQ.BindEventLoginStatus(this, ReceiveLoginStatus);
            // cw.SetHttpWeb(ch);//设置WebQQ对象当前使用的网络对象
            pictureBox1.Image = SmartQQ.Login_GetQRCode();
        }
    }
}
