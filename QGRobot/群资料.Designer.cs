﻿namespace QGRobot
{
    partial class qunziliao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(qunziliao));
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.lvfriendguan = new System.Windows.Forms.ListView();
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendQQ = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendnick = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendcity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonFriendSend = new System.Windows.Forms.Button();
            this.textBoxFriendSend = new System.Windows.Forms.TextBox();
            this.textBoxFriendChat = new System.Windows.Forms.TextBox();
            this.tabPage8.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.groupBox7);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(658, 324);
            this.tabPage8.TabIndex = 8;
            this.tabPage8.Text = "聊天框";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.lvfriendguan);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(658, 324);
            this.tabPage7.TabIndex = 7;
            this.tabPage7.Text = "群员列表";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // lvfriendguan
            // 
            this.lvfriendguan.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader5,
            this.friendid,
            this.friendQQ,
            this.friendnick,
            this.friendcity,
            this.columnHeader17,
            this.columnHeader18});
            this.lvfriendguan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvfriendguan.FullRowSelect = true;
            this.lvfriendguan.GridLines = true;
            this.lvfriendguan.Location = new System.Drawing.Point(3, 3);
            this.lvfriendguan.Name = "lvfriendguan";
            this.lvfriendguan.Size = new System.Drawing.Size(652, 318);
            this.lvfriendguan.TabIndex = 1;
            this.lvfriendguan.UseCompatibleStateImageBehavior = false;
            this.lvfriendguan.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "编号";
            // 
            // friendid
            // 
            this.friendid.Text = "昵称";
            this.friendid.Width = 86;
            // 
            // friendQQ
            // 
            this.friendQQ.Text = "QQ号";
            this.friendQQ.Width = 119;
            // 
            // friendnick
            // 
            this.friendnick.Text = "性别";
            this.friendnick.Width = 97;
            // 
            // friendcity
            // 
            this.friendcity.Text = "所在国家";
            this.friendcity.Width = 81;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "所在省份";
            this.columnHeader17.Width = 115;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "所在城市";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(666, 350);
            this.tabControl1.TabIndex = 18;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonFriendSend);
            this.groupBox7.Controls.Add(this.textBoxFriendSend);
            this.groupBox7.Controls.Add(this.textBoxFriendChat);
            this.groupBox7.Location = new System.Drawing.Point(6, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(386, 312);
            this.groupBox7.TabIndex = 18;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "聊天信息";
            // 
            // buttonFriendSend
            // 
            this.buttonFriendSend.Location = new System.Drawing.Point(306, 179);
            this.buttonFriendSend.Name = "buttonFriendSend";
            this.buttonFriendSend.Size = new System.Drawing.Size(47, 101);
            this.buttonFriendSend.TabIndex = 12;
            this.buttonFriendSend.Text = "发送";
            this.buttonFriendSend.UseVisualStyleBackColor = true;
            // 
            // textBoxFriendSend
            // 
            this.textBoxFriendSend.AcceptsTab = true;
            this.textBoxFriendSend.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxFriendSend.Location = new System.Drawing.Point(6, 179);
            this.textBoxFriendSend.Multiline = true;
            this.textBoxFriendSend.Name = "textBoxFriendSend";
            this.textBoxFriendSend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxFriendSend.Size = new System.Drawing.Size(274, 101);
            this.textBoxFriendSend.TabIndex = 11;
            // 
            // textBoxFriendChat
            // 
            this.textBoxFriendChat.AcceptsTab = true;
            this.textBoxFriendChat.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxFriendChat.Location = new System.Drawing.Point(0, 20);
            this.textBoxFriendChat.MaxLength = 0;
            this.textBoxFriendChat.Multiline = true;
            this.textBoxFriendChat.Name = "textBoxFriendChat";
            this.textBoxFriendChat.ReadOnly = true;
            this.textBoxFriendChat.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxFriendChat.Size = new System.Drawing.Size(353, 136);
            this.textBoxFriendChat.TabIndex = 10;
            // 
            // qunziliao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(681, 365);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "qunziliao";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "群资料";
            this.Load += new System.EventHandler(this.群资料_Load);
            this.tabPage8.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ListView lvfriendguan;
        private System.Windows.Forms.ColumnHeader friendid;
        private System.Windows.Forms.ColumnHeader friendQQ;
        private System.Windows.Forms.ColumnHeader friendnick;
        private System.Windows.Forms.ColumnHeader friendcity;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.GroupBox groupBox7;
        internal System.Windows.Forms.Button buttonFriendSend;
        private System.Windows.Forms.TextBox textBoxFriendSend;
        private System.Windows.Forms.TextBox textBoxFriendChat;


    }
}