﻿namespace QGRobot
{
     partial  class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();

            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.formtyt = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menu_Notify = new System.Windows.Forms.ToolStripMenuItem();
            this.隐藏ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logclass = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.清空ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.停止ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.开始日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.好友管理 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.gorugclass = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.管理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新群列表ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.群测试 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsqqnum = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.lsApps = new System.Windows.Forms.ListView();
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Plugmanagemen = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.安装ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.刷新ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.设置ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.删除ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.目录ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.lvgroup = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.lvfriend = new System.Windows.Forms.ListView();
            this.friendid = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendQQ = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendnick = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.friendcity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Outputrecord = new System.Windows.Forms.TabPage();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tblogon = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label7 = new System.Windows.Forms.Label();
            this.fujian = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.body = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.subject = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.recipient = new System.Windows.Forms.TextBox();
            this.recipienta = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.sendera = new System.Windows.Forms.TextBox();
            this.password = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.buttonDiscussSend = new System.Windows.Forms.Button();
            this.textBoxDiscussSend = new System.Windows.Forms.TextBox();
            this.textBoxDiscussChat = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.listBoxDiscuss = new System.Windows.Forms.ListBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.buttonGroupSend = new System.Windows.Forms.Button();
            this.textBoxGroupSend = new System.Windows.Forms.TextBox();
            this.textBoxGroupChat = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.listBoxGroup = new System.Windows.Forms.ListBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.buttonFriendSend = new System.Windows.Forms.Button();
            this.textBoxFriendSend = new System.Windows.Forms.TextBox();
            this.textBoxFriendChat = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.listBoxFriend = new System.Windows.Forms.ListBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.lsLogs = new System.Windows.Forms.ListBox();
            this.formtyt.SuspendLayout();
            this.logclass.SuspendLayout();
            this.好友管理.SuspendLayout();
            this.gorugclass.SuspendLayout();
            this.群测试.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.Plugmanagemen.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.Outputrecord.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.formtyt;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "QGRobot";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // formtyt
            // 
            this.formtyt.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Notify,
            this.隐藏ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.formtyt.Name = "contextMenuStrip1";
            this.formtyt.Size = new System.Drawing.Size(101, 70);
            // 
            // menu_Notify
            // 
            this.menu_Notify.Name = "menu_Notify";
            this.menu_Notify.Size = new System.Drawing.Size(100, 22);
            this.menu_Notify.Text = "显示";
            this.menu_Notify.Click += new System.EventHandler(this.menu_Notify_Click);
            // 
            // 隐藏ToolStripMenuItem
            // 
            this.隐藏ToolStripMenuItem.Name = "隐藏ToolStripMenuItem";
            this.隐藏ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.隐藏ToolStripMenuItem.Text = "隐藏";
            this.隐藏ToolStripMenuItem.Click += new System.EventHandler(this.隐藏ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // logclass
            // 
            this.logclass.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.清空ToolStripMenuItem,
            this.停止ToolStripMenuItem,
            this.开始日志ToolStripMenuItem});
            this.logclass.Name = "contextMenuStrip2";
            this.logclass.Size = new System.Drawing.Size(125, 70);
            // 
            // 清空ToolStripMenuItem
            // 
            this.清空ToolStripMenuItem.Name = "清空ToolStripMenuItem";
            this.清空ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.清空ToolStripMenuItem.Text = "清空日志";
            this.清空ToolStripMenuItem.Click += new System.EventHandler(this.清空ToolStripMenuItem_Click);
            // 
            // 停止ToolStripMenuItem
            // 
            this.停止ToolStripMenuItem.Name = "停止ToolStripMenuItem";
            this.停止ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.停止ToolStripMenuItem.Text = "停止日志";
            this.停止ToolStripMenuItem.Click += new System.EventHandler(this.停止ToolStripMenuItem_Click);
            // 
            // 开始日志ToolStripMenuItem
            // 
            this.开始日志ToolStripMenuItem.Name = "开始日志ToolStripMenuItem";
            this.开始日志ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.开始日志ToolStripMenuItem.Text = "开始日志";
            this.开始日志ToolStripMenuItem.Click += new System.EventHandler(this.开始日志ToolStripMenuItem_Click);
            // 
            // 好友管理
            // 
            this.好友管理.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.toolStripMenuItem2});
            this.好友管理.Name = "群信息";
            this.好友管理.Size = new System.Drawing.Size(153, 70);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem1.Text = "发送消息";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItem2.Text = "刷新好友列表";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // gorugclass
            // 
            this.gorugclass.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.管理ToolStripMenuItem,
            this.刷新群列表ToolStripMenuItem});
            this.gorugclass.Name = "群信息";
            this.gorugclass.Size = new System.Drawing.Size(137, 48);
            // 
            // 管理ToolStripMenuItem
            // 
            this.管理ToolStripMenuItem.Name = "管理ToolStripMenuItem";
            this.管理ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.管理ToolStripMenuItem.Text = "管理";
            this.管理ToolStripMenuItem.Click += new System.EventHandler(this.管理ToolStripMenuItem_Click);
            // 
            // 刷新群列表ToolStripMenuItem
            // 
            this.刷新群列表ToolStripMenuItem.Name = "刷新群列表ToolStripMenuItem";
            this.刷新群列表ToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.刷新群列表ToolStripMenuItem.Text = "刷新群列表";
            this.刷新群列表ToolStripMenuItem.Click += new System.EventHandler(this.刷新群列表ToolStripMenuItem_Click);
            // 
            // 群测试
            // 
            this.群测试.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem3,
            this.toolStripMenuItem4});
            this.群测试.Name = "群信息";
            this.群测试.Size = new System.Drawing.Size(137, 48);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem3.Text = "管理";
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(136, 22);
            this.toolStripMenuItem4.Text = "刷新群列表";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsqqnum,
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel2});
            this.statusStrip1.Location = new System.Drawing.Point(0, 470);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(739, 22);
            this.statusStrip1.TabIndex = 17;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsqqnum
            // 
            this.tsqqnum.Name = "tsqqnum";
            this.tsqqnum.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(0, 17);
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(12, 17);
            this.toolStripStatusLabel2.Text = "t";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.lsApps);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(709, 429);
            this.tabPage10.TabIndex = 10;
            this.tabPage10.Text = "插件管理";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // lsApps
            // 
            this.lsApps.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader5,
            this.columnHeader6});
            this.lsApps.ContextMenuStrip = this.Plugmanagemen;
            this.lsApps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lsApps.FullRowSelect = true;
            this.lsApps.GridLines = true;
            this.lsApps.Location = new System.Drawing.Point(3, 3);
            this.lsApps.Name = "lsApps";
            this.lsApps.Size = new System.Drawing.Size(703, 423);
            this.lsApps.TabIndex = 4;
            this.lsApps.UseCompatibleStateImageBehavior = false;
            this.lsApps.View = System.Windows.Forms.View.Details;
            this.lsApps.SelectedIndexChanged += new System.EventHandler(this.lsApps_SelectedIndexChanged);
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "插件名";
            this.columnHeader12.Width = 119;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "状态";
            this.columnHeader13.Width = 142;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "介绍";
            this.columnHeader14.Width = 81;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "版本号";
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "作者";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = ".NET";
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "位置";
            // 
            // Plugmanagemen
            // 
            this.Plugmanagemen.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.安装ToolStripMenuItem,
            this.刷新ToolStripMenuItem,
            this.设置ToolStripMenuItem,
            this.删除ToolStripMenuItem,
            this.目录ToolStripMenuItem});
            this.Plugmanagemen.Name = "Plugmanagemen";
            this.Plugmanagemen.Size = new System.Drawing.Size(101, 114);
            // 
            // 安装ToolStripMenuItem
            // 
            this.安装ToolStripMenuItem.Name = "安装ToolStripMenuItem";
            this.安装ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.安装ToolStripMenuItem.Text = "安装";
            this.安装ToolStripMenuItem.Click += new System.EventHandler(this.安装ToolStripMenuItem_Click);
            // 
            // 刷新ToolStripMenuItem
            // 
            this.刷新ToolStripMenuItem.Name = "刷新ToolStripMenuItem";
            this.刷新ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.刷新ToolStripMenuItem.Text = "刷新";
            this.刷新ToolStripMenuItem.Click += new System.EventHandler(this.刷新ToolStripMenuItem_Click);
            // 
            // 设置ToolStripMenuItem
            // 
            this.设置ToolStripMenuItem.Name = "设置ToolStripMenuItem";
            this.设置ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.设置ToolStripMenuItem.Text = "设置";
            this.设置ToolStripMenuItem.Click += new System.EventHandler(this.设置ToolStripMenuItem_Click);
            // 
            // 删除ToolStripMenuItem
            // 
            this.删除ToolStripMenuItem.Name = "删除ToolStripMenuItem";
            this.删除ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.删除ToolStripMenuItem.Text = "删除";
            this.删除ToolStripMenuItem.Click += new System.EventHandler(this.删除ToolStripMenuItem_Click);
            // 
            // 目录ToolStripMenuItem
            // 
            this.目录ToolStripMenuItem.Name = "目录ToolStripMenuItem";
            this.目录ToolStripMenuItem.Size = new System.Drawing.Size(100, 22);
            this.目录ToolStripMenuItem.Text = "目录";
            this.目录ToolStripMenuItem.Click += new System.EventHandler(this.目录ToolStripMenuItem_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.lvgroup);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage8.Size = new System.Drawing.Size(709, 429);
            this.tabPage8.TabIndex = 8;
            this.tabPage8.Text = "群列表";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // lvgroup
            // 
            this.lvgroup.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader10});
            this.lvgroup.ContextMenuStrip = this.gorugclass;
            this.lvgroup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvgroup.FullRowSelect = true;
            this.lvgroup.GridLines = true;
            this.lvgroup.Location = new System.Drawing.Point(3, 3);
            this.lvgroup.Name = "lvgroup";
            this.lvgroup.Size = new System.Drawing.Size(703, 423);
            this.lvgroup.TabIndex = 2;
            this.lvgroup.UseCompatibleStateImageBehavior = false;
            this.lvgroup.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "编号";
            this.columnHeader1.Width = 86;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "群号";
            this.columnHeader2.Width = 119;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "昵称";
            this.columnHeader3.Width = 142;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "数量";
            this.columnHeader4.Width = 81;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "群主";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.lvfriend);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(709, 429);
            this.tabPage7.TabIndex = 7;
            this.tabPage7.Text = "好友列表";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // lvfriend
            // 
            this.lvfriend.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.friendid,
            this.friendQQ,
            this.friendnick,
            this.friendcity});
            this.lvfriend.ContextMenuStrip = this.好友管理;
            this.lvfriend.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvfriend.FullRowSelect = true;
            this.lvfriend.GridLines = true;
            this.lvfriend.Location = new System.Drawing.Point(3, 3);
            this.lvfriend.Name = "lvfriend";
            this.lvfriend.Size = new System.Drawing.Size(703, 423);
            this.lvfriend.TabIndex = 1;
            this.lvfriend.UseCompatibleStateImageBehavior = false;
            this.lvfriend.View = System.Windows.Forms.View.Details;
            // 
            // friendid
            // 
            this.friendid.Text = "编号";
            this.friendid.Width = 86;
            // 
            // friendQQ
            // 
            this.friendQQ.Text = "QQ号";
            this.friendQQ.Width = 119;
            // 
            // friendnick
            // 
            this.friendnick.Text = "昵称";
            this.friendnick.Width = 142;
            // 
            // friendcity
            // 
            this.friendcity.Text = "所在城市";
            this.friendcity.Width = 81;
            // 
            // Outputrecord
            // 
            this.Outputrecord.Controls.Add(this.groupBox10);
            this.Outputrecord.Location = new System.Drawing.Point(4, 22);
            this.Outputrecord.Name = "Outputrecord";
            this.Outputrecord.Padding = new System.Windows.Forms.Padding(3);
            this.Outputrecord.Size = new System.Drawing.Size(709, 429);
            this.Outputrecord.TabIndex = 6;
            this.Outputrecord.Text = "输出记录";
            this.Outputrecord.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.tblogon);
            this.groupBox10.Location = new System.Drawing.Point(5, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(794, 420);
            this.groupBox10.TabIndex = 12;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "输出记录";
            // 
            // tblogon
            // 
            this.tblogon.ContextMenuStrip = this.logclass;
            this.tblogon.Location = new System.Drawing.Point(1, 13);
            this.tblogon.Multiline = true;
            this.tblogon.Name = "tblogon";
            this.tblogon.ReadOnly = true;
            this.tblogon.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tblogon.Size = new System.Drawing.Size(690, 410);
            this.tblogon.TabIndex = 16;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage5.Controls.Add(this.label7);
            this.tabPage5.Controls.Add(this.fujian);
            this.tabPage5.Controls.Add(this.label5);
            this.tabPage5.Controls.Add(this.body);
            this.tabPage5.Controls.Add(this.label6);
            this.tabPage5.Controls.Add(this.subject);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.recipient);
            this.tabPage5.Controls.Add(this.recipienta);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.label1);
            this.tabPage5.Controls.Add(this.sendera);
            this.tabPage5.Controls.Add(this.password);
            this.tabPage5.Controls.Add(this.button3);
            this.tabPage5.Controls.Add(this.button2);
            this.tabPage5.Controls.Add(this.button1);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(709, 429);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "系统设置";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(142, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "附件路径：";
            // 
            // fujian
            // 
            this.fujian.Location = new System.Drawing.Point(205, 96);
            this.fujian.Name = "fujian";
            this.fujian.Size = new System.Drawing.Size(100, 21);
            this.fujian.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(142, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 13;
            this.label5.Text = "内容：";
            // 
            // body
            // 
            this.body.Location = new System.Drawing.Point(205, 55);
            this.body.Name = "body";
            this.body.Size = new System.Drawing.Size(100, 21);
            this.body.TabIndex = 12;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(142, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 12);
            this.label6.TabIndex = 11;
            this.label6.Text = "主题：";
            // 
            // subject
            // 
            this.subject.Location = new System.Drawing.Point(205, 19);
            this.subject.Name = "subject";
            this.subject.Size = new System.Drawing.Size(100, 21);
            this.subject.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(323, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 12);
            this.label4.TabIndex = 9;
            this.label4.Text = "抄件人：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(508, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "收件人：";
            // 
            // recipient
            // 
            this.recipient.Location = new System.Drawing.Point(579, 55);
            this.recipient.Name = "recipient";
            this.recipient.Size = new System.Drawing.Size(100, 21);
            this.recipient.TabIndex = 7;
            // 
            // recipienta
            // 
            this.recipienta.Location = new System.Drawing.Point(394, 55);
            this.recipienta.Name = "recipienta";
            this.recipienta.Size = new System.Drawing.Size(100, 21);
            this.recipienta.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(508, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "SMTP密码：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(323, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "邮箱账号：";
            // 
            // sendera
            // 
            this.sendera.Location = new System.Drawing.Point(394, 19);
            this.sendera.Name = "sendera";
            this.sendera.Size = new System.Drawing.Size(100, 21);
            this.sendera.TabIndex = 2;
            // 
            // password
            // 
            this.password.Location = new System.Drawing.Point(579, 19);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(100, 21);
            this.password.TabIndex = 2;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(555, 177);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 1;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(555, 148);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(555, 108);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "发邮件";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage4.Controls.Add(this.groupBox9);
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(709, 429);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "讨论组";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.buttonDiscussSend);
            this.groupBox9.Controls.Add(this.textBoxDiscussSend);
            this.groupBox9.Controls.Add(this.textBoxDiscussChat);
            this.groupBox9.Location = new System.Drawing.Point(201, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(341, 370);
            this.groupBox9.TabIndex = 16;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "聊天信息";
            // 
            // buttonDiscussSend
            // 
            this.buttonDiscussSend.Location = new System.Drawing.Point(286, 260);
            this.buttonDiscussSend.Name = "buttonDiscussSend";
            this.buttonDiscussSend.Size = new System.Drawing.Size(47, 101);
            this.buttonDiscussSend.TabIndex = 12;
            this.buttonDiscussSend.Text = "发送";
            this.buttonDiscussSend.UseVisualStyleBackColor = true;
            this.buttonDiscussSend.Click += new System.EventHandler(this.buttonDiscussSend_Click);
            // 
            // textBoxDiscussSend
            // 
            this.textBoxDiscussSend.AcceptsTab = true;
            this.textBoxDiscussSend.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxDiscussSend.Location = new System.Drawing.Point(6, 260);
            this.textBoxDiscussSend.Multiline = true;
            this.textBoxDiscussSend.Name = "textBoxDiscussSend";
            this.textBoxDiscussSend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDiscussSend.Size = new System.Drawing.Size(274, 101);
            this.textBoxDiscussSend.TabIndex = 11;
            // 
            // textBoxDiscussChat
            // 
            this.textBoxDiscussChat.AcceptsTab = true;
            this.textBoxDiscussChat.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxDiscussChat.Location = new System.Drawing.Point(6, 15);
            this.textBoxDiscussChat.MaxLength = 0;
            this.textBoxDiscussChat.Multiline = true;
            this.textBoxDiscussChat.Name = "textBoxDiscussChat";
            this.textBoxDiscussChat.ReadOnly = true;
            this.textBoxDiscussChat.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDiscussChat.Size = new System.Drawing.Size(329, 239);
            this.textBoxDiscussChat.TabIndex = 10;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.listBoxDiscuss);
            this.groupBox6.Location = new System.Drawing.Point(6, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(189, 370);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "讨论组列表";
            // 
            // listBoxDiscuss
            // 
            this.listBoxDiscuss.FormattingEnabled = true;
            this.listBoxDiscuss.ItemHeight = 12;
            this.listBoxDiscuss.Location = new System.Drawing.Point(6, 21);
            this.listBoxDiscuss.Name = "listBoxDiscuss";
            this.listBoxDiscuss.Size = new System.Drawing.Size(177, 340);
            this.listBoxDiscuss.TabIndex = 9;
            this.listBoxDiscuss.SelectedIndexChanged += new System.EventHandler(this.listBoxDiscuss_SelectedIndexChanged);
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage3.Controls.Add(this.groupBox5);
            this.tabPage3.Controls.Add(this.groupBox4);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(709, 429);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "群";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.buttonGroupSend);
            this.groupBox5.Controls.Add(this.textBoxGroupSend);
            this.groupBox5.Controls.Add(this.textBoxGroupChat);
            this.groupBox5.Location = new System.Drawing.Point(201, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(341, 370);
            this.groupBox5.TabIndex = 17;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "聊天信息";
            // 
            // buttonGroupSend
            // 
            this.buttonGroupSend.Location = new System.Drawing.Point(286, 260);
            this.buttonGroupSend.Name = "buttonGroupSend";
            this.buttonGroupSend.Size = new System.Drawing.Size(47, 101);
            this.buttonGroupSend.TabIndex = 12;
            this.buttonGroupSend.Text = "发送";
            this.buttonGroupSend.UseVisualStyleBackColor = true;
            this.buttonGroupSend.Click += new System.EventHandler(this.buttonGroupSend_Click);
            // 
            // textBoxGroupSend
            // 
            this.textBoxGroupSend.AcceptsTab = true;
            this.textBoxGroupSend.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxGroupSend.Location = new System.Drawing.Point(6, 260);
            this.textBoxGroupSend.Multiline = true;
            this.textBoxGroupSend.Name = "textBoxGroupSend";
            this.textBoxGroupSend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxGroupSend.Size = new System.Drawing.Size(274, 101);
            this.textBoxGroupSend.TabIndex = 11;
            // 
            // textBoxGroupChat
            // 
            this.textBoxGroupChat.AcceptsTab = true;
            this.textBoxGroupChat.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxGroupChat.Location = new System.Drawing.Point(6, 15);
            this.textBoxGroupChat.MaxLength = 0;
            this.textBoxGroupChat.Multiline = true;
            this.textBoxGroupChat.Name = "textBoxGroupChat";
            this.textBoxGroupChat.ReadOnly = true;
            this.textBoxGroupChat.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxGroupChat.Size = new System.Drawing.Size(329, 239);
            this.textBoxGroupChat.TabIndex = 10;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.listBoxGroup);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(189, 370);
            this.groupBox4.TabIndex = 13;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "群列表";
            // 
            // listBoxGroup
            // 
            this.listBoxGroup.FormattingEnabled = true;
            this.listBoxGroup.HorizontalScrollbar = true;
            this.listBoxGroup.ItemHeight = 12;
            this.listBoxGroup.Location = new System.Drawing.Point(6, 21);
            this.listBoxGroup.Name = "listBoxGroup";
            this.listBoxGroup.Size = new System.Drawing.Size(177, 340);
            this.listBoxGroup.TabIndex = 9;
            this.listBoxGroup.SelectedIndexChanged += new System.EventHandler(this.listBoxGroup_SelectedIndexChanged);
            this.listBoxGroup.DoubleClick += new System.EventHandler(this.listBoxGroup_DoubleClick);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox7);
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(709, 429);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "好友";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.buttonFriendSend);
            this.groupBox7.Controls.Add(this.textBoxFriendSend);
            this.groupBox7.Controls.Add(this.textBoxFriendChat);
            this.groupBox7.Location = new System.Drawing.Point(201, 6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(341, 370);
            this.groupBox7.TabIndex = 17;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "聊天信息";
            // 
            // buttonFriendSend
            // 
            this.buttonFriendSend.Location = new System.Drawing.Point(286, 260);
            this.buttonFriendSend.Name = "buttonFriendSend";
            this.buttonFriendSend.Size = new System.Drawing.Size(47, 101);
            this.buttonFriendSend.TabIndex = 12;
            this.buttonFriendSend.Text = "发送";
            this.buttonFriendSend.UseVisualStyleBackColor = true;
            this.buttonFriendSend.Click += new System.EventHandler(this.buttonFriendSend_Click);
            // 
            // textBoxFriendSend
            // 
            this.textBoxFriendSend.AcceptsTab = true;
            this.textBoxFriendSend.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxFriendSend.Location = new System.Drawing.Point(6, 260);
            this.textBoxFriendSend.Multiline = true;
            this.textBoxFriendSend.Name = "textBoxFriendSend";
            this.textBoxFriendSend.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxFriendSend.Size = new System.Drawing.Size(274, 101);
            this.textBoxFriendSend.TabIndex = 11;
            // 
            // textBoxFriendChat
            // 
            this.textBoxFriendChat.AcceptsTab = true;
            this.textBoxFriendChat.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxFriendChat.Location = new System.Drawing.Point(6, 15);
            this.textBoxFriendChat.MaxLength = 0;
            this.textBoxFriendChat.Multiline = true;
            this.textBoxFriendChat.Name = "textBoxFriendChat";
            this.textBoxFriendChat.ReadOnly = true;
            this.textBoxFriendChat.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxFriendChat.Size = new System.Drawing.Size(329, 239);
            this.textBoxFriendChat.TabIndex = 10;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.listBoxFriend);
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(189, 370);
            this.groupBox3.TabIndex = 12;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "好友列表";
            // 
            // listBoxFriend
            // 
            this.listBoxFriend.HorizontalScrollbar = true;
            this.listBoxFriend.ItemHeight = 12;
            this.listBoxFriend.Location = new System.Drawing.Point(6, 21);
            this.listBoxFriend.Name = "listBoxFriend";
            this.listBoxFriend.Size = new System.Drawing.Size(177, 340);
            this.listBoxFriend.TabIndex = 9;
            this.listBoxFriend.SelectedIndexChanged += new System.EventHandler(this.listBoxFriend_SelectedIndexChanged);
            this.listBoxFriend.DoubleClick += new System.EventHandler(this.listBoxFriend_DoubleClick);
            // 
            // tabControl1
            // 
            this.tabControl1.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.Outputrecord);
            this.tabControl1.Controls.Add(this.tabPage7);
            this.tabControl1.Controls.Add(this.tabPage8);
            this.tabControl1.Controls.Add(this.tabPage10);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(717, 455);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.lsLogs);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(709, 429);
            this.tabPage1.TabIndex = 11;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // lsLogs
            // 
            this.lsLogs.FormattingEnabled = true;
            this.lsLogs.ItemHeight = 12;
            this.lsLogs.Location = new System.Drawing.Point(6, 12);
            this.lsLogs.Name = "lsLogs";
            this.lsLogs.ScrollAlwaysVisible = true;
            this.lsLogs.Size = new System.Drawing.Size(697, 316);
            this.lsLogs.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 492);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(900, 530);
            this.MinimumSize = new System.Drawing.Size(650, 530);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "QGRobot";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing_1);
            this.Load += new System.EventHandler(this.FormLogin_Load);
            this.SizeChanged += new System.EventHandler(this.MainForm_SizeChanged);
            this.formtyt.ResumeLayout(false);
            this.logclass.ResumeLayout(false);
            this.好友管理.ResumeLayout(false);
            this.gorugclass.ResumeLayout(false);
            this.群测试.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.Plugmanagemen.ResumeLayout(false);
            this.tabPage8.ResumeLayout(false);
            this.tabPage7.ResumeLayout(false);
            this.Outputrecord.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip formtyt;
        private System.Windows.Forms.ToolStripMenuItem menu_Notify;
        private System.Windows.Forms.ToolStripMenuItem 隐藏ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.StatusStrip statusStrip1;
        public System.Windows.Forms.ToolStripStatusLabel tsqqnum;
        private System.Windows.Forms.ContextMenuStrip logclass;
        private System.Windows.Forms.ToolStripMenuItem 清空ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 停止ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 开始日志ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip gorugclass;
        private System.Windows.Forms.ToolStripMenuItem 管理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新群列表ToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip 好友管理;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ContextMenuStrip 群测试;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.ListView lsApps;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.ListView lvgroup;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ListView lvfriend;
        private System.Windows.Forms.ColumnHeader friendid;
        private System.Windows.Forms.ColumnHeader friendQQ;
        private System.Windows.Forms.ColumnHeader friendnick;
        private System.Windows.Forms.ColumnHeader friendcity;
        private System.Windows.Forms.TabPage Outputrecord;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox9;
        internal System.Windows.Forms.Button buttonDiscussSend;
        private System.Windows.Forms.TextBox textBoxDiscussSend;
        private System.Windows.Forms.TextBox textBoxDiscussChat;
        private System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.ListBox listBoxDiscuss;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox5;
        internal System.Windows.Forms.Button buttonGroupSend;
        private System.Windows.Forms.TextBox textBoxGroupSend;
        private System.Windows.Forms.TextBox textBoxGroupChat;
        private System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.ListBox listBoxGroup;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox7;
        internal System.Windows.Forms.Button buttonFriendSend;
        private System.Windows.Forms.TextBox textBoxFriendSend;
        private System.Windows.Forms.TextBox textBoxFriendChat;
        private System.Windows.Forms.GroupBox groupBox3;
        public System.Windows.Forms.ListBox listBoxFriend;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        public System.Windows.Forms.TextBox tblogon;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox sendera;
        public System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox recipient;
        private System.Windows.Forms.TextBox recipienta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox body;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox subject;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox fujian;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TabPage tabPage1;
        public System.Windows.Forms.ListBox lsLogs;
        private System.Windows.Forms.ContextMenuStrip Plugmanagemen;
        private System.Windows.Forms.ToolStripMenuItem 安装ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 刷新ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 设置ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 删除ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 目录ToolStripMenuItem;
        private System.Windows.Forms.ColumnHeader columnHeader6;
    }
}

