﻿using Jurassic;
using Jurassic.Library;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using System.Windows.Forms;
using QGWebQQSDK;

namespace QGRobot
{
    public partial class qunziliao : Form 
    {
        public qunziliao()
        {
            InitializeComponent();
        }
    
    
        internal void ReNewListBoxGroup()
        {
            #region 获取群成员
            try
            {
                string strJson = SmartQQ.GetGroupMemberList(MainForm.qunuin);
                QGWebQQSDK.JSONObject jo = new QGWebQQSDK.JSONObject(strJson);
                JSONArray ja = jo.getJSONObject("result").getJSONArray("minfo");
                lvfriendguan.Items.Clear();
                for (int i = 0; i < ja.length(); i++)
                {
                    string nick = ja.getJSONObject(i).getString("nick");
                    string province = ja.getJSONObject(i).getString("province").ToString();
                    string gender = ja.getJSONObject(i).getString("gender").ToString();
                    long uin = ja.getJSONObject(i).getLong("uin");
                    string country = ja.getJSONObject(i).getString("country").ToString();
                    string city = ja.getJSONObject(i).getString("city").ToString();
                    //  ListViewItem item = new ListViewItem(new string[] { nick.ToString(), province, gender, uin, country, city });
                    //  lbweb.Items.Add(item);
                    if (gender == "male")
                    {
                        gender = "男";
                    }
                    else if (gender == "female")
                    {
                        gender = "女";
                    }
                    else if (gender == "unknown")
                    {
                        gender = "未知的";
                    }

                    lvfriendguan.Items.Add(new ListViewItem(new string[] {
                 i.ToString (),
                  nick.ToString (),
               //    SmartQQ.Info_Reauin ( uin), 
               uin.ToString (),
                gender.ToString() , 
                province.ToString(),
                  country.ToString (),
                  city .ToString ()
              }));
                }
            }
            catch { }
            #endregion
        }
        private void 群资料_Load(object sender, EventArgs e)
        {
            ReNewListBoxGroup();
          
          
           // ReNewListBoxGroup();
         }

   
    }
}
