﻿using Aocwes.MainConsole;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using QGGWXSDK;
using WXCSTest;

namespace WXBotTest
{
    public partial class Form1 : Form
    {
        WXWebAPI api = new WXWebAPI();
        public Form1()
        {
            InitializeComponent();
            ///设置api的回调函数
            api.LogEvent = SetAppendTextInfo;
            api.ImageEvent = SetQRImageInfo;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ///新建线程运行
            Thread mythread = new Thread(api.Run);
            mythread.Start();
        }

        private void SetAppendTextInfo(string _info)
        {
            if (InvokeRequired)
            {
                Action<string> actionDelegate = new Action<string>(SetAppendTextInfo);
                BeginInvoke(actionDelegate, _info);
            }
            else
            {
                this.textBox1.AppendText("\r\n" + _info);
            }
        }

        private void SetQRImageInfo(Image _image)
        {
            if (InvokeRequired)
            {
                Action<Image> actionDelegate = new Action<Image>(SetQRImageInfo);
                BeginInvoke(actionDelegate, _image);
            }
            else
            {
                this.pictureBox1.Image = _image;
            }
        }

     


        public void SendDataToWX(string str)
        {
            api.SendMsgToFD(str);
        }

        //启动端口，开始监听，如果收到http消息，转发给威信
        void WriteY()
        {
            DDResiveServer.BugTicketEventHandler myDelegate = new DDResiveServer.BugTicketEventHandler(SendDataToWX);
            HttpListener hl = new HttpListener(myDelegate);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ///新建线程运行
            Thread mythread = new Thread(api.Run);
            mythread.Start();
            //Thread mythread = new Thread(WriteY);
            //mythread.Start();
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //if (api != null)
            //{
            //    api.Stop();
            //    api.LogEvent = null;
            //    api.ImageEvent = null;
            //}
        }
        private void button2_Click(object sender, EventArgs e)
        {
            Form2 dlg = new Form2();
            if (dlg.ShowDialog() == DialogResult.OK)
            {
                api.AddAutoMsg(dlg.ReciveMsg, dlg.SendMsg);
            }

        }
        private void button3_Click(object sender, EventArgs e)
        {
            //if (api != null)
            //{
            //    api.Stop();
            //    api.LogEvent = null;
            //    api.ImageEvent = null;
            //}
            //Application.Run(new MainForm());//打开窗体2
          MainForm dlg = new MainForm();
          dlg.Show();
          //  this.Close();
        }
    }
}
