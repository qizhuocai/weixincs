﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Windows;
using System.Threading;
using System.IO;
using QGGWXSDK.WXObject.WXMsgObj;
using QGGWXSDK.WXObject;
using QGGWXSDK;
using System.Collections;
using System.Drawing;
using Aocwes.MainConsole;
using QGGWXSDK.Log;
using QGGWXSDK.WXWB;

namespace WXCSTest
{

    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
            //  api.LogEvent = SetAppendTextInfo;
            QGGWXSDK.Log.SysLog.LogEvent = SetAppendTextInfo;

        }
        public static string nameuser;
        #region 更新公众号界面列表
        /// <summary>
        /// 更新主界面的公众号列表
        /// </summary>
        internal void ReNewListBoxGroup()
        {
            //   SendOnlog("更新主界面的QQ群列表入口");
            //SysOnlog("更新主界面的QQ群列表入口");

            lvgroup.Items.Clear();

            foreach (KeyValuePair<string, MemberGroup> GroupList in WXWebAPI.Group)
            {
                //if (((IList)StaticValues.special_users).Contains(GroupList.Value.UserName))
                //{
                //if (((int)GroupList.Value.VerifyFlag & 8) != 0)
                //{
                //    listBoxGroup.Items.Add(GroupList.Key
                // + "::" + GroupList.Value.City
                //+ "::" + GroupList.Value.NickName
                //+ "::" + GroupList.Value.City
                //+ "::" + GroupList.Value.UserName.ToString()
                //   );
                lvgroup.Items.Add(new ListViewItem(new string[] {
                    GroupList.Key.ToString(),
                  GroupList.Value.Signature , 
                    GroupList.Value.NickName,
                  GroupList.Value.City ,
                    GroupList.Value.UserName.ToString()
              
              }));
            }


            //    }


        }
        #endregion
        #region 更新主界面的好友列表
        /// <summary>
        /// 更新主界面的好友列表
        /// </summary>
        internal void ReNewListBoxFriend()
        {
            //   listBoxFriend.Items.Clear();
            lvfriend.Items.Clear();
            int i = 0;
            foreach (KeyValuePair<string, MemUser> GList in WXWebAPI.User)
            {
                i++;
                lvfriend.Items.Add(new ListViewItem(new string[] {
                    GList.Key.ToString(),
                  GList.Value.RemarkName , 
                    GList.Value.NickName,
                  GList.Value.Signature ,
                    GList.Value.UserName.ToString()

              }));
            }
            // }

        }
        #endregion
        #region 更新主界面的群聊列表
        /// <summary>
        /// 更新主界面的群聊列表
        /// </summary>
        internal void ReNewListBoxDiscuss()
        {
            //listBoxDiscuss.Items.Clear();
            lvgroupchat.Items.Clear();
            foreach (KeyValuePair<string, GroupChat> GroupList in WXWebAPI.Chat)
            {
                lvgroupchat.Items.Add(new ListViewItem(new string[] {
                    GroupList.Key.ToString(),
                  GroupList.Value.PYInitial.ToString () , 
                    GroupList.Value.NickName.ToString (),
                  GroupList.Value.HeadImgUrl.ToString () ,
                    GroupList.Value.UserName.ToString()
              
              }));
            }
            //}
        }
        #endregion
        #region 更新主界面的其他应用列表
        /// <summary>
        /// 更新主界面的其他应用列表
        /// </summary>
        internal void ReNewListOtherMem()
        {
            //listBoxDiscuss.Items.Clear();
            lvOtherMem.Items.Clear();
            foreach (KeyValuePair<string, OtherMem> GroupList in WXWebAPI.OtherMe)
            {
                lvOtherMem.Items.Add(new ListViewItem(new string[] {
                    GroupList.Key.ToString(),
                  GroupList.Value.PYInitial.ToString () , 
                    GroupList.Value.NickName.ToString (),
                  GroupList.Value.HeadImgUrl .ToString (),
                    GroupList.Value.UserName.ToString()
              }));
            }
            //}
        }
        #endregion

        #region 委托数据
        private void SetAppendTextInfo(string _info)
        {
            if (InvokeRequired)
            {
                Action<string> actionDelegate = new Action<string>(SetAppendTextInfo);
                BeginInvoke(actionDelegate, _info);
            }
            else
            {
                this.tblogon.AppendText("\r\n" + _info);
            }
        }
        #endregion



        #region 控件事件

        public void button1_Click(object sender, EventArgs e)
        {

            string send = sendera.Text.ToString().Trim();
            string pass = password.Text.ToString().Trim();
            string bod = body.Text.ToString().Trim();
            string sub = subject.Text.ToString().Trim();
            string recip = recipient.Text.ToString().Trim();
            string recipa = recipienta.Text.ToString().Trim();
            string fu = fujian.Text.ToString().Trim();
            if (!send.EndsWith("@qq.com"))
            {
                send += "@qq.com";
            }
            if (!recip.EndsWith("@qq.com"))
            {
                recip += "@qq.com";
            }
            if (!recipa.EndsWith("@qq.com"))
            {
                recipa += "@qq.com";
            }
            if (send == "" || pass == "" || bod == "" || sub == "" || recip == "")
            {
                MessageBox.Show("不能空");
                return;
            }
            //else if (Mail.SendMail(send, recip, bod, sub, pass))
            //{
            //    SendOnlog(send + "对" + recip + "发送邮件成功");
            //}
            //else
            //{
            //    SendOnlog(send + "对" + recip + "发送邮件失败");
            //}
            //string  a = "270954350";
            //  textBox1.Text = SmartQQ.Info_RealQQ(a);

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //    string a = "270954350";textBox1.Text = SmartQQ.Info_Reauin(a);
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            toolStripStatusLabel2.Text = "现在时间：" + DateTime.Now.ToString();


        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            ReNewListBoxGroup(); ReNewListOtherMem();
            ReNewListBoxFriend(); ReNewListBoxDiscuss();
        }

        private void 管理ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #region

            if (lvgroupchat.Created == true)
            {

                List<string> c = new List<string>();
                for (int i = 0; i < lvgroupchat.SelectedItems.Count; i++)
                {
                    var item = lvgroupchat.SelectedItems[i];

                    //  c .Add(item.SubItems[0].Text);
                    nameuser = item.SubItems[0].Text.Trim();

                }
                //  SmartQQ.Info_ZDGroupListInfo(qunuin);
                qunziliao aa = new qunziliao();
                aa.ShowDialog();

                //  SendOnlog("打开群员界面中。。。");
            }
            #endregion
        }

        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
        #endregion

        string qunuin;
        string qunuina;
        private void button4_Click(object sender, EventArgs e)
        {
            if (!string .IsNullOrEmpty (tbfriendmsg.Text ))
            {
                WXMsgg msg = new WXMsgg();
                foreach (var item in WXWebAPI.UserGG)
                {
                    msg.Uin = WXWebAPI.Uin.ToString();
                    msg.Sid = WXWebAPI.Sid;
                    msg.From = item.Value.UserName;
                    msg.Msg = tbfriendmsg.Text .ToString().Trim();
                    msg.Readed = false;
                    msg.To = qunuin;
                    msg.Type = 1;
                    msg.Time = DateTime.Now;
                }
                WXWebAPI.SendMsg(msg.Msg, msg.From, msg.To, msg.Type, msg.Uin, msg.Sid);
            }

        }

        private void lvfriend_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lvfriend.Created == true)
            {
                if (lvfriend.SelectedIndices != null && lvfriend.SelectedIndices.Count > 0)
                {
                    ListView.SelectedIndexCollection c = lvfriend.SelectedIndices;
                    qunuin = lvfriend.Items[c[0]].Text;
                   // SysLog.SendMseege("测试", qunuin);
                }
            }
            for (int i = 0; i < lvfriend.SelectedItems.Count; i++)
            {
                var item = lvfriend.SelectedItems[i];
                //  c .Add(item.SubItems[0].Text);
                label8.Text  = "正在跟好友："+item.SubItems[2].Text.Trim()+" 聊天中";
              //  SysLog.SendMseege("测试1", qunuina);

            }
        }
    }

}