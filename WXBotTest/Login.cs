﻿using QGGWXSDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WXCSTest;

namespace WXBotTest
{
    public partial class Login : Form
    {
        WXWebAPI api = new WXWebAPI();
        public Login()
        {
            InitializeComponent();
            ///设置api的回调函数
            api.LogEvent = SetAppendTextInfo;
            api.ImageEvent = SetQRImageInfo;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //新建线程运行
            Thread mythread = new Thread(api.Run);
            mythread.Start();
        }
        private void SetAppendTextInfo(string _info)
        {
            if (_info == "qizhuocai")
            {
                WXCSTest.MainForm ds = new WXCSTest.MainForm();
                ds.Show();
                this.Close();
            }
            if (InvokeRequired)
            {
                Action<string> actionDelegate = new Action<string>(SetAppendTextInfo);
                BeginInvoke(actionDelegate, _info);
            }
            else
            {
                this.label1.Text = _info;
                if (label1.Text.IndexOf("自己的消息处理函数收到") == 1)
                {
                    button2_Click(null ,null );
                }
            }
           
        }

        private void SetQRImageInfo(Image _image)
        {
            if (InvokeRequired)
            {
                Action<Image> actionDelegate = new Action<Image>(SetQRImageInfo);
                BeginInvoke(actionDelegate, _image);
            }
            else
            {
                this.pictureBox1 .Image = _image;
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            ///新建线程运行
            Thread mythread = new Thread(api.Run);
            mythread.Start();
            //Thread mythread = new Thread(WriteY);
            //mythread.Start();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MainForm ds = new MainForm();
            ds.Show();
            this.Close();
        }
    }
}
