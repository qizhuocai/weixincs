﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WXBotTest
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(textBox1.Text)==false && string.IsNullOrEmpty(textBox2.Text)==false )
            {
                reciveMsg = textBox1.Text;
                sendMsg = textBox2.Text;
                this.DialogResult=DialogResult.OK;
            }
        }

        string reciveMsg;

        public string ReciveMsg
        {
            get { return reciveMsg; }
        }
        string sendMsg;

        public string SendMsg
        {
            get { return sendMsg; }
        }
    }
}
